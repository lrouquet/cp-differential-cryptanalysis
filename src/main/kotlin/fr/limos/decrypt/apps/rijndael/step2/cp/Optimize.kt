package fr.limos.decrypt.apps.rijndael.step2.cp

import com.github.rloic.phd.core.arrays.Matrix
import com.github.rloic.phd.core.arrays.Tensor3
import com.github.rloic.phd.core.arrays.filled
import com.github.rloic.phd.core.arrays.matrixOfNulls
import com.github.rloic.phd.core.cryptography.rijndael.Rijndael
import fr.limos.decrypt.abstractxor.AbstractXORHelper
import fr.limos.decrypt.abstractxor.Consistency
import fr.limos.decrypt.propagators.MDSPropagator
import fr.limos.decrypt.propagators.XORSum
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap
import org.chocosolver.solver.Model
import org.chocosolver.solver.constraints.Constraint
import org.chocosolver.solver.constraints.extension.Tuples
import org.chocosolver.solver.variables.BoolVar
import org.chocosolver.solver.variables.IntVar

@Suppress("NonAsciiCharacters")
class Optimize(
   private val Nr: Int,
   private val Nb: Int,
   private val Nk: Int,
   private val objStep1: Int,
   private val isSbColumn: (Int) -> Boolean,
) {

   data class ByteVar(val δ: IntVar, val Δ: BoolVar, val name: String)
   data class SBox(val input: ByteVar, val output: ByteVar, val p: IntVar)

   private val NRows = 4
   private val abstractXorEquations = mutableListOf<Array<IntVar>>()

   val m = Model()

   private fun ik(i: Int, k: Int) = i * Nb + k

   fun byteVar(name: String): ByteVar {
      val concrete = m.intVar("δ$name", 0, 255)
      val abstract = m.boolVar("Δ$name")
      m.arithm(concrete, "!=", 0).reifyWith(abstract)
      return ByteVar(concrete, abstract, name)
   }

   val X: Tensor3<ByteVar>
   val colX: Matrix<IntVar>

   val SX: Tensor3<ByteVar>

   val Y: Tensor3<ByteVar>
   val colY: Matrix<IntVar>

   val Z: Tensor3<ByteVar>
   val colZ: Matrix<IntVar>

   val WK: Matrix<ByteVar>
   val colWK: Array<IntVar>

   val decomposition = Int2ObjectArrayMap<MutableList<IntVar>>()
   val parentsOf = Int2ObjectArrayMap<MutableList<IntVar>>()

   val sboxes = mutableListOf<SBox>()

   val objStep2: IntVar

   val nbActives: Array<IntVar>
   val sboxColumns: Array<IntVar>

   val intVarToIgnore = mutableListOf<IntVar>()
   val probabilities = mutableListOf<IntVar>()

   init {
      X = Tensor3(Nr, NRows, Nb) { i, j, k -> byteVar("X[$i, $j, $k]") }
      colX = columnsOf("X", X)
      SX = X.map(::subCell)
      Y = Tensor3(Nr, NRows, Nb) { i, j, k -> SX[i, j, (k + Rijndael.shifts[Nb - 4, j]) % Nb] }
      colY = columnsOf("Y", Y)
      Z = Tensor3(Nr - 1) { i -> mixColumn(Y[i]) }
      colZ = columnsOf("Z", Z)

      WK = expandedKey()
      colWK = columnsOf("WK", WK)

      nbActives = activesOf(colY, colWK)
      sboxColumns = List(Nr + 1) { decomposition[nbActives[it].id]!! }
         .flatten()
         .toTypedArray()

      for (ik in Nk until Nb * (Nr + 1)) {
         m.post(Constraint("XOR Columns", XORSum(colWK[ik], colWK[ik - 1], colWK[ik - Nk])))
      }

      for (i in 0 until Nr - 1) {
         for (k in 0 until Nb) {
            m.post(Constraint("MDS", MDSPropagator(colY[i, k], colZ[i, k])))
            m.post(Constraint("XOR Columns", XORSum(colX[i + 1, k], colZ[i, k], colWK[ik(i + 1, k)])))
            for (j in 0 until NRows) {
               postXor(X[i + 1, j, k], Z[i, j, k], WK[j, ik(i + 1, k)])
            }
         }
      }

      advancedMds()

      objStep2 = m.intVar(6 * objStep1, 7 * objStep1)
      m.sum(nbActives, "=", objStep1).post()
      m.sum(sboxColumns, "=", objStep1).post()
      m.sum(sboxes.map { it.input.Δ } .toTypedArray(), "=", objStep1).post()
      m.sum(probabilities.toTypedArray(), "=", objStep2).post()

      AbstractXORHelper.postAbsXor(m, Consistency.Gac, abstractXorEquations)
      m.setObjective(Model.MINIMIZE, objStep2)
   }

   private fun activesOf(y: Matrix<IntVar>, wk: Array<IntVar>): Array<IntVar> {
      fun selectTextSboxColForRound(r: Int): Array<IntVar> = if (r < Nr) Array(Nb) { k -> y[r, k] } else emptyArray()
      fun selectKeySboxColForRound(r: Int): List<IntVar> = Array(Nb) { k ->
         if (isSbColumn(ik(r, k))) wk[ik(r, k)] else null
      }.filterNotNull()

      return Array(Nr + 1) { i ->
         val roundSboxes = selectTextSboxColForRound(i) + selectKeySboxColForRound(i)
         val nbActives = sumIntVar(roundSboxes, "nbActives[$i]")
         nbActives
      }
   }

   private fun columnsOf(name: String, state: Tensor3<ByteVar>) =
      Matrix(state.dim1, state.dim3) { i, k -> sumAbsVar(Array(state.dim2) { j -> state[i, j, k] }, "$name[$i, $k]") }

   private fun columnsOf(name: String, state: Matrix<ByteVar>) =
      Array(state.dim2) { k -> sumAbsVar(Array(state.dim1) { j -> state[j, k] }, "$name[$k]") }

   private fun sumAbsVar(vars: Array<ByteVar>, name: String? = null): IntVar {
      var lb = 0
      var ub = 0

      for (variable in vars) {
         lb += variable.Δ.lb
         ub += variable.Δ.ub
      }

      val sum = if (name != null) m.intVar(name, lb, ub) else m.intVar(lb, ub)
      m.sum(vars.map(ByteVar::Δ).toTypedArray(), "=", sum).post()
      val sumDecomposition = decomposition.getOrPut(sum.id) { mutableListOf() }
      for (variable in vars) {
         parentsOf.getOrPut(variable.δ.id) { mutableListOf() } += sum
         parentsOf.getOrPut(variable.Δ.id) { mutableListOf() } += sum
         if (variable.δ !in sumDecomposition) {
            sumDecomposition += variable.δ
         }
         if (variable.Δ !in sumDecomposition) {
            sumDecomposition += variable.Δ
         }
      }

      return sum
   }

   private fun sumAbsVar(vars: Array<BoolVar>, name: String? = null): IntVar {
      var lb = 0
      var ub = 0

      for (variable in vars) {
         lb += variable.lb
         ub += variable.ub
      }

      val sum = if (name != null) m.intVar(name, lb, ub) else m.intVar(lb, ub)
      m.sum(vars, "=", sum).post()
      val sumDecomposition = decomposition.getOrPut(sum.id) { mutableListOf() }
      for (variable in vars) {
         parentsOf.getOrPut(variable.id) { mutableListOf() } += sum
         if (variable !in sumDecomposition) {
            sumDecomposition += variable
         }
      }

      return sum
   }

   private fun sumIntVar(vars: Array<IntVar>, name: String? = null): IntVar {
      var lb = 0
      var ub = 0

      for (variable in vars) {
         lb += variable.lb
         ub += variable.ub
      }

      val sum = if (name != null) m.intVar(name, lb, ub) else m.intVar(lb, ub)
      m.sum(vars, "=", sum).post()
      val sumDecomposition = decomposition.getOrPut(sum.id) { mutableListOf() }
      for (variable in vars) {
         parentsOf.getOrPut(variable.id) {  mutableListOf() } += sum
         if (variable !in sumDecomposition) {
            sumDecomposition += variable
         }
      }

      return sum
   }

   private fun subCell(sbIn: ByteVar): ByteVar {
      val sbOut = byteVar("S(${sbIn.name})")
      val p = m.intVar(intArrayOf(0, 6, 7))
      sboxes += SBox(sbIn, sbOut, p)
      probabilities += p
      m.table(arrayOf(sbIn.δ, sbOut.δ, p), Rijndael.SBOX_TUPLES, "FC").post()
      m.arithm(sbIn.Δ, "=", sbOut.Δ).post()
      m.arithm(p, "!=", 0).reifyWith(sbIn.Δ)
      return sbOut
   }

   private fun mixColumn(Yi: Matrix<ByteVar>): Matrix<ByteVar> {
      val Y2 = Yi.map { glMul(it, 2) }
      val Y3 = Matrix(NRows, Nb) { j, k -> Yi[j, k] xor Y2[j, k] }

      val Zn = matrixOfNulls<ByteVar>(NRows, Nb)
      for (k in 0 until Nb) {
         Zn[0, k] = Y2[0, k] xor Y3[1, k] xor Yi[2, k] xor Yi[3, k]
         Zn[1, k] = Yi[0, k] xor Y2[1, k] xor Y3[2, k] xor Yi[3, k]
         Zn[2, k] = Yi[0, k] xor Yi[1, k] xor Y2[2, k] xor Y3[3, k]
         Zn[3, k] = Y3[0, k] xor Yi[1, k] xor Yi[2, k] xor Y2[3, k]
      }

      val Zi = Zn.filled()

      val Z09 = Zi.map { glMul(it, 9) }
      val Z11 = Zi.map { glMul(it, 11) }
      val Z13 = Zi.map { glMul(it, 13) }
      val Z14 = Zi.map { glMul(it, 14) }

      for (k in 0 until Nb) {
         postEq(Yi[0, k], Z14[0, k] xorC Z11[1, k] xorC Z13[2, k] xorC Z09[3, k])
         postEq(Yi[1, k], Z09[0, k] xorC Z14[1, k] xorC Z11[2, k] xorC Z13[3, k])
         postEq(Yi[2, k], Z13[0, k] xorC Z09[1, k] xorC Z14[2, k] xorC Z11[3, k])
         postEq(Yi[3, k], Z11[0, k] xorC Z13[1, k] xorC Z09[2, k] xorC Z14[3, k])
      }

      return Zi
   }

   private fun advancedMds() {
      for (i1 in 0 until Nr - 1) {
         for (k1 in 0 until Nb) {
            for (i2 in 0 until Nr - 1) {
               for (k2 in 0 until Nb) {
                  if (i2 > i1 || (i2 == i1 && k2 > k1)) {
                     val diffY = sumAbsVar(Array(NRows) { j -> absXor(Y[i1, j, k1].Δ, Y[i2, j, k2].Δ) })
                     m.post(Constraint("", XORSum(diffY, colY[i1, k1], colY[i2, k2])))
                     val diffZ = sumAbsVar(Array(NRows) { j -> absXor(Z[i1, j, k1].Δ, Z[i2, j, k2].Δ) })
                     m.post(Constraint("", XORSum(diffZ, colZ[i1, k1], colZ[i2, k2])))

                     m.post(Constraint("MDS Decomposed", MDSPropagator(diffY, diffZ)))
                  }
               }
            }
         }
      }
   }


   private fun postEq(lhs: ByteVar, rhs: ByteVar) {
      m.arithm(lhs.δ, "=", rhs.δ).post()
      m.arithm(lhs.Δ, "=", rhs.Δ).post()
      abstractXorEquations += arrayOf(lhs.Δ, rhs.Δ)
   }

   private fun glMul(input: ByteVar, k: Int): ByteVar {

      if (k == 0) return ByteVar(m.intVar(0), m.boolVar(false), "ZERO/FALSE")
      if (k == 1) return input

      val mulTables = arrayOfNulls<Tuples>(15)

      mulTables[2] = Rijndael.mul2
      mulTables[3] = Rijndael.mul3
      mulTables[9] = Rijndael.mul9
      mulTables[11] = Rijndael.mul11
      mulTables[13] = Rijndael.mul13
      mulTables[14] = Rijndael.mul14

      val tuples = mulTables[k]

      val output = byteVar("${k}${input.name}")
      m.table(input.δ, output.δ, tuples, "FC").post()
      m.arithm(input.Δ, "=", output.Δ).post()

      return output
   }

   private fun postXor(lhs: ByteVar, rhs: ByteVar, res: ByteVar) {
      m.table(arrayOf(lhs.δ, rhs.δ, res.δ), Rijndael.XOR_TUPLES, "FC").post()
      abstractXorEquations += arrayOf(lhs.Δ, rhs.Δ, res.Δ)
      m.sum(arrayOf(lhs.Δ, rhs.Δ, res.Δ), "!=", 1).post()
   }

   private infix fun ByteVar.xor(rhs: ByteVar): ByteVar {
      val res = byteVar("$name ^ ${rhs.name}")
      postXor(this, rhs, res)
      return res
   }

   private infix fun ByteVar.xorC(rhs: ByteVar): ByteVar {
      val res = byteVar("$name ^ ${rhs.name}")
      m.table(arrayOf(this.δ, rhs.δ, res.δ), Rijndael.XOR_TUPLES, "FC").post()
      return res
   }

   private fun absXor(lhs: BoolVar, rhs: BoolVar): BoolVar {
      val res = m.boolVar("${lhs.name} ^ ${rhs.name}")
      abstractXorEquations += arrayOf(lhs, rhs, res)
      m.sum(arrayOf(lhs,res,rhs), "!=", 1).post()
      return res
   }

   private fun expandedKey(): Matrix<ByteVar> {
      val WK = matrixOfNulls<ByteVar>(NRows, (Nr + 1) * Nb)

      for (ik in 0 until Nk) {
         for (j in 0 until NRows) {
            WK[j, ik] = byteVar("WK[$j, $ik]")
         }
      }

      for (ik in Nk until Nb * (Nr + 1)) {
         if (ik % Nk == 0) {
            for (j in 0 until NRows) {
               WK[j, ik] = WK[j, ik - Nk]!! xor subCell(WK[(j + 1) % NRows, ik - 1]!!)
            }
         } else if (Nk > 6 && ik % Nk == 4) {
            for (j in 0 until NRows) {
               WK[j, ik] = WK[j, ik - Nk]!! xor subCell(WK[j, ik - 1]!!)
            }
         } else {
            for (j in 0 until NRows) {
               WK[j, ik] = WK[j, ik - Nk]!! xor WK[j, ik - 1]!!
            }
         }
      }

      return WK.filled()
   }


}