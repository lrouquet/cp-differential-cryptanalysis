package fr.limos.decrypt.apps.rijndael.step2.sat

import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Configuration
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Solution
import com.github.rloic.phd.core.files.csv.CSV
import com.github.rloic.phd.core.mzn.MznModelBuilder
import com.github.rloic.phd.core.mzn.PartialMznModel
import com.github.rloic.phd.core.mzn.SearchStrategy
import com.github.rloic.phd.core.mzn.ValueSelector
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import fr.limos.decrypt.apps.rijndael.step1.helpers.KeySchedule
import fr.limos.decrypt.apps.rijndael.step1.common.sat.EnumerationSolutionParser
import fr.limos.decrypt.apps.rijndael.step1.common.sat.PartialAssignmentExtractor
import fr.limos.decrypt.utils.IntPtr
import fr.limos.decrypt.utils.div
import java.io.File
import javaextensions.io.dir

// Usage: kscript run.kt CIPHER_BITS=VALUE KEY_BITS=VALUE Nr=VALUE objStep1=VALUE
// CIPHER_BITS in {128, 160, 192, 224, 256}
// KEY_BITS in {128, 160, 192, 224, 256}
// Nr >= 3
// objStep1 >= 0
fun main(_args_: Array<String>) {

    val args = parseArgs(_args_)

    val config = Configuration(
        args.expectArgument("Nr").toInt(),
        args.expectArgument("TextSize").toInt(),
        args.expectArgument("KeySize").toInt(),
        args.expectArgument("ObjStep1").toInt()
    )

    val minizinc = MiniZincBinary(args.expectArgument("MiniZinc"))
    val picat = PicatBinary(
        args.expectArgument("Picat"),
        args.expectArgument("PicatFzn"),
        minizinc
    )

    println("Rijndael/Step2/OptimizeSatGLMS20:Choco $config")
    val sat = File("sat")

    val solutionParser = EnumerationSolutionParser(config)
    val assignmentExtractor = PartialAssignmentExtractor(solutionParser)

    val modelParts = listOf(PartialMznModel(sat / "Rijndael.mzn"), KeySchedule.generate(config, overwrite = true))
    val decisionVars = PartialMznModel(sat / "DecisionVars.mzn")
    val forbidSolution = PartialMznModel(sat / "ForbidSolution.mzn")

    val builder = MznModelBuilder.PartialAssignment(
        modelParts, decisionVars, SearchStrategy.Smallest, ValueSelector.InDomainMin, forbidSolution,
        assignmentExtractor::extract
    )

    val target =
        dir("sat/tmp/enumerate") / "tmp-enumerate-sat--${config.textSize}-${config.keySize}_${config.Nr}.mzn"
    val mznModel = builder.build(target)

    val minObjStep1 = config.objStep1
    val nbSol = IntPtr(0)
    val bestProb = IntPtr(Int.MIN_VALUE)
    val bestObjStep1 = IntPtr(Int.MAX_VALUE)

    val step1Solutions = mutableListOf<Solution>()
    for(mznSolution in picat.enumerate(mznModel, data = mapOf())) {
        step1Solutions += solutionParser.parse(mznSolution)
    }

    for (solution in step1Solutions) {
        // OptimizeForSatGLMS20(solution, nbSol, bestProb, bestObjStep1)
    }

    println(CSV.formatLine("nb_sols", "obj_step_1", "best_probability").content)
    println("nb_sols, obj_step_1, best_probability")
    if (nbSol.value == 0) {
        println(CSV.formatLine(0, minObjStep1, "-INFTY").content)
    } else {
        println(CSV.formatLine(nbSol, minObjStep1, "2^{$bestProb}").content)
    }

}