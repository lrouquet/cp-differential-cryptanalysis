package fr.limos.decrypt.apps.rijndael.step1

import java.io.File

class MinRoundsConfig(file: File) {

   private val minRounds = mutableMapOf<Int, MutableMap<Int, List<Int>>>()

   init {
      file.forEachLine(Charsets.UTF_8, ::parse)
   }

   private fun parse(line: String) {
      val (NbNk, allValues) = line.split(':')
      val (Nb, Nk) = NbNk.split('-').map(String::toInt)
      val values = allValues.split(',').map(String::toInt)
      val resNb = minRounds.getOrPut(Nb) { mutableMapOf() }
      resNb[Nk] = values
   }

   operator fun get(Nb: Int, Nk: Int, round: Int): Int {
      val minForNb = minRounds[Nb * 32]

      if (minForNb == null) return 0

      val minForNbNk = minForNb[Nk * 32]

      if (minForNbNk == null) return 0

      if (minForNbNk.size < round - 3) return 0

      if (round < 3) return 0

      return minForNbNk[round - 3]
   }

}