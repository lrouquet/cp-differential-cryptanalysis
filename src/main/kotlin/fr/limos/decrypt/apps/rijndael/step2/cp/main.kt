package fr.limos.decrypt.apps.rijndael.step2.cp

import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Configuration
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs
import fr.limos.decrypt.apps.rijndael.step1.MinRoundsConfig
import fr.limos.decrypt.strategy.DomOverWDegWithDecomposition
import javaextensions.io.getResourceAsFile
import org.chocosolver.solver.search.strategy.Search
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDegRef
import org.chocosolver.solver.variables.BoolVar
import org.chocosolver.solver.variables.IntVar
import java.io.File

fun main(_args_: Array<String>) {

   val args = parseArgs(_args_)

   val config = Configuration(
      args.expectArgument("Nr").toInt(),
      args.expectArgument("TextSize").toInt(),
      args.expectArgument("KeySize").toInt(),
      args.expectArgument("ObjStep1").toInt()
   )

   val Nk = config.Nk
   val Nb = config.Nb
   val Nr = config.Nr

   val delegate = Optimize(config.Nr, config.Nb, config.Nk, config.objStep1, config::isSbColumn)

   val m = delegate.m
   val solver = delegate.m.solver

   val MIN_OBJ_STEP1 = MinRoundsConfig(getResourceAsFile("min_rounds.txt"))

   val cumulSum = arrayOfNulls<IntVar>(Nr + 1)
   cumulSum[0] = delegate.nbActives[0]
   delegate.decomposition[cumulSum[0]!!.id] = delegate.decomposition[delegate.nbActives[0].id]
   for (i in 1..Nr) {
      val name = cumulSum[i - 1]!!.name + " + " + delegate.nbActives[i].name
      cumulSum[i] = m.intVar(name, MIN_OBJ_STEP1[Nb, Nk, i], config.objStep1)
      delegate.decomposition[cumulSum[i]!!.id] =
         (delegate.decomposition[cumulSum[i - 1]!!.id] + delegate.decomposition[delegate.nbActives[i].id]).toMutableList()
      m.arithm(cumulSum[i - 1], "+", delegate.nbActives[i], "=", cumulSum[i]).post()
   }

   val complementSum = arrayOfNulls<IntVar>(Nr)
   complementSum[Nr - 1] = delegate.nbActives[Nr]
   delegate.decomposition[complementSum[Nr - 1]!!.id] = delegate.decomposition[delegate.nbActives[Nr].id]
   for (i in Nr - 2 downTo 0) {
      val name: String = delegate.nbActives[i + 1].name + " + " + complementSum[i + 1]!!.name
      complementSum[i] = m.intVar(name, 0, config.objStep1 - MIN_OBJ_STEP1[Nb, Nk, i])
      delegate.decomposition[cumulSum[i]!!.id] =
         (delegate.decomposition[cumulSum[i + 1]!!.id] + delegate.decomposition[delegate.nbActives[i + 1].id]).toMutableList()
      m.arithm(delegate.nbActives[i + 1], "+", complementSum[i + 1], "=", complementSum[i]).post()
   }

   for (i in 0 until Nr) {
      m.arithm(cumulSum[i], "+", complementSum[i], "=", config.objStep1).post()
   }

   for (sbox in delegate.sboxes) {
      val parents = mutableListOf<IntVar>()
      parents += delegate.parentsOf[sbox.input.Δ.id] ?: emptyList()
      parents += delegate.parentsOf[sbox.output.Δ.id] ?: emptyList()
      delegate.parentsOf[sbox.input.δ.id] = parents
      delegate.parentsOf[sbox.output.δ.id] = parents
      delegate.parentsOf[sbox.p.id] = parents
   }

   val absOrder = mutableListOf<BoolVar>()
   for (i in 0 until Nr + 1) {
      if (i < Nr) {
         for (k in 0 until Nb) {
            for (j in 0 until 4) {
               absOrder += delegate.Y[i, j, k].Δ
            }

            val ik = i * Nb + k
            if (config.isSbColumn(ik)) {
               for (j in 0 until 4) {
                  absOrder += delegate.WK[j, ik].Δ
               }
            }
         }
      }
   }

   val sboxes = delegate.sboxes.flatMap { listOf(it.input.δ, it.output.δ, it.p) }

   val cVars =
      (delegate.X.deepFlatten() +
      delegate.SX.deepFlatten() +
      delegate.Z.deepFlatten() +
      delegate.WK.deepFlatten()).map(Optimize.ByteVar::δ).toTypedArray()

   solver.setSearch(
      DomOverWDegWithDecomposition((delegate.nbActives + delegate.sboxColumns), delegate.decomposition, 0, IntDomainMin()),
      DomOverWDegRef(absOrder.toTypedArray(), 0, IntDomainMin()),
      DomOverWDegWithDecomposition(sboxes.toTypedArray(), delegate.parentsOf, 0, IntDomainMin()),
      Search.inputOrderLBSearch(*cVars)
   )
   solver.setSearch(Search.lastConflict(solver.getSearch()))

   while (solver.solve()) {
      solver.printShortStatistics()
   }
   solver.printShortStatistics()

}


