package fr.limos.decrypt.apps.rijndael.step1.common.cp

import com.github.rloic.phd.core.arrays.Matrix
import com.github.rloic.phd.core.arrays.Tensor3
import com.github.rloic.phd.core.arrays.tensor5OfNulls
import com.github.rloic.phd.core.cryptography.rijndael.Rijndael
import fr.limos.decrypt.abstractxor.AbstractXORHelper
import fr.limos.decrypt.abstractxor.Consistency
import fr.limos.decrypt.propagators.MDSPropagator
import fr.limos.decrypt.propagators.XORSum
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap
import org.chocosolver.solver.Model
import org.chocosolver.solver.constraints.Constraint
import org.chocosolver.solver.variables.BoolVar
import org.chocosolver.solver.variables.IntVar

@Suppress("NonAsciiCharacters")
class Step1(
   private val Nr: Int,
   private val Nb: Int,
   private val Nk: Int,
   private val isSbColumn: (Int) -> Boolean
) {

   private val NRows = 4
   private val xorEquations = mutableListOf<Array<IntVar>>()

   val m = Model()

   val sboxes = mutableListOf<BoolVar>()

   val ΔX: Tensor3<BoolVar>
   val colX: Matrix<IntVar>

   val ΔSX: Tensor3<BoolVar>

   val ΔY: Tensor3<BoolVar>
   val colY: Matrix<IntVar>

   val ΔZ: Tensor3<BoolVar>
   val colZ: Matrix<IntVar>

   val ΔWK: Matrix<BoolVar>
   val colWK: Array<IntVar>

   val decomposition = Int2ObjectArrayMap<MutableList<IntVar>>()

   val nbActives: Array<IntVar>
   val sboxColumns: Array<IntVar>
   val intVarToIgnore = mutableListOf<IntVar>()

   init {
      ΔX = Tensor3(Nr, NRows, Nb) { i, j, k -> m.boolVar("ΔX[$i, $j, $k]") }
      colX = columnsOf("colX", ΔX)
      ΔSX = Tensor3(Nr, NRows, Nb) { i, j, k -> subCell(ΔX[i, j, k]) }

      ΔY = Tensor3(Nr, NRows, Nb) { i, j, k -> ΔSX[i, j, (k + Rijndael.shifts[Nb - 4, j]) % Nb] }
      colY = columnsOf("colY", ΔY)

      ΔZ = Tensor3(Nr - 1, NRows, Nb) { i, j, k -> m.boolVar("ΔZ[$i, $j, $k]") }
      colZ = columnsOf("colZ", ΔZ)

      ΔWK = expandedKey()
      colWK = columnsOf("colWK", ΔWK)

      nbActives = activesOf(colY, colWK)
      sboxColumns = List(Nr + 1) { decomposition[nbActives[it].id]!! }.flatten().toTypedArray()

      // KeySchedule
      for (ik in Nk until Nb * (Nr + 1)) {
         postXorCol(colWK[ik], colWK[ik - Nk], colWK[ik - 1])
      }

      // MDS
      for (i in 0 until Nr - 1) {
         for (k in 0 until Nb) {
            m.post(Constraint("MDS", MDSPropagator(colY[i, k], colZ[i, k])))
         }
      }

      // ARK
      for (i in 0 until Nr - 1) {
         for (k in 0 until Nb) {
            for (j in 0 until NRows) {
               postXor(ΔX[i + 1, j, k], ΔZ[i, j, k], ΔWK[j, (i + 1) * Nb + k])
            }
            postXorCol(colX[i + 1, k], colZ[i, k], colWK[(i + 1) * Nb + k])
         }
      }

      check(nbActives.sumBy { it.ub } == sboxes.size)

      advancedMds()
      AbstractXORHelper.postAbsXor(m, Consistency.Gac, xorEquations)
   }

   private fun activesOf(y: Matrix<IntVar>, wk: Array<IntVar>): Array<IntVar> {
      fun selectTextSboxColForRound(r: Int) = if (r < Nr) Array(Nb) { k -> y[r, k] } else emptyArray()
      fun selectKeySboxColForRound(r: Int) = Array(Nb) { k ->
         val ik = r * Nb + k
         if (isSbColumn(ik)) wk[ik] else null
      }.filterNotNull()

      return Array(Nr + 1) { i ->
         val roundSboxes = selectTextSboxColForRound(i) + selectKeySboxColForRound(i)
         val nbActives = sumIntVar(roundSboxes, "nbActives[$i]")
         nbActives
      }
   }

   private fun columnsOf(name: String, state: Tensor3<BoolVar>) =
      Matrix(state.dim1, state.dim3) { i, k -> sumBoolVar(Array(state.dim2) { j -> state[i, j, k] }, "$name[$i, $k]") }

   private fun columnsOf(name: String, state: Matrix<BoolVar>) =
      Array(state.dim2) { k -> sumBoolVar(Array(state.dim1) { j -> state[j, k] }, "$name[$k]") }

   private fun subCell(x: BoolVar): BoolVar {
      sboxes += x
      val sx = m.boolVar("S(${x.name})")
      m.arithm(x, "=", sx).post()
      return sx
   }

   private fun advancedMds() {
      val diffY = tensor5OfNulls<BoolVar>(Nr - 1, Nb, NRows, Nr - 1, Nb)
      val diffZ = tensor5OfNulls<BoolVar>(Nr - 1, Nb, NRows, Nr - 1, Nb)

      for (i1 in 0 until Nr - 1) {
         for (k1 in 0..3) {
            for (i2 in 0 until Nr - 1) {
               for (k2 in 0..3) {
                  if (i2 > i1 || (i2 == i1 && k2 > k1)) {
                     for (j in 0..3) {
                        diffY[i1, k1, j, i2, k2] =
                           m.boolVar(String.format("diffY[%d][%d][%d][%d][%d]", i1, k1, j, i2, k2))
                        postXor(diffY[i1, k1, j, i2, k2]!!, ΔY[i1, j, k1], ΔY[i2, j, k2])

                        diffZ[i1, k1, j, i2, k2] =
                           m.boolVar(String.format("diffZ[%d][%d][%d][%d][%d]", i1, k1, j, i2, k2))
                        postXor(diffZ[i1, k1, j, i2, k2]!!, ΔZ[i1, j, k1], ΔZ[i2, j, k2])
                     }

                     val colDiffY = Array(NRows) { j -> diffY[i1, k1, j, i2, k2]!! }
                     intVarToIgnore += colDiffY
                     val colDiffYSum = sumBoolVar(colDiffY, "colDiffY[$i1, $k1, $i2, $k2]")
                     postXorCol(colDiffYSum, colY[i1, k1], colY[i2, k2])
                     intVarToIgnore += colDiffYSum

                     val colDiffZ = Array(NRows) { j -> diffZ[i1, k1, j, i2, k2]!! }
                     intVarToIgnore += colDiffZ
                     val colDiffZSum = sumBoolVar(colDiffZ, "colDiffZ[$i1, $k1, $i2, $k2]")
                     postXorCol(colDiffZSum, colZ[i1, k1], colZ[i2, k2])
                     intVarToIgnore += colDiffZSum

                     m.post(Constraint("Decomposed MDS", MDSPropagator(colDiffYSum, colDiffZSum)))
                  }
               }
            }
         }
      }
   }

   private fun postXor(vararg variables: BoolVar) {
      xorEquations += variables.toList().toTypedArray()
   }

   private fun expandedKey(): Matrix<BoolVar> {
      val ΔWK = Matrix(NRows, (Nr + 1) * Nb) { j, ik -> m.boolVar("ΔWK[$j, $ik]") }
      for (ik in Nk until Nb * (Nr + 1)) {
         if (ik % Nk == 0) {
            for (j in 0 until NRows) {
               postXor(ΔWK[j, ik], ΔWK[j, ik - Nk], subCell(ΔWK[(j + 1) % NRows, ik - 1]))
            }
         } else if (Nk > 6 && ik % Nk == 4) {
            for (j in 0 until NRows) {
               postXor(ΔWK[j, ik], ΔWK[j, ik - Nk], subCell(ΔWK[j, ik - 1]))
            }
         } else {
            for (j in 0 until NRows) {
               postXor(ΔWK[j, ik], ΔWK[j, ik - Nk], ΔWK[j, ik - 1])
            }
         }
      }
      return ΔWK
   }

   private fun postXorCol(A: IntVar, B: IntVar, C: IntVar) {
      m.post(Constraint("XORSUM", XORSum(A, B, C)))
   }

   private fun sumBoolVar(vars: Array<BoolVar>, name: String? = null): IntVar {
      var lb = 0
      var ub = 0

      for (variable in vars) {
         lb += variable.lb
         ub += variable.ub
      }

      val sum = if (name != null) m.intVar(name, lb, ub) else m.intVar(lb, ub)
      m.sum(vars, "=", sum).post()
      val sumDecomposition = decomposition.getOrPut(sum.id) { mutableListOf() }
      for (variable in vars) {
         if (variable !in sumDecomposition) {
            sumDecomposition += variable
         }
      }

      return sum
   }

   private fun sumIntVar(vars: Array<IntVar>, name: String? = null): IntVar {
      var lb = 0
      var ub = 0

      for (variable in vars) {
         lb += variable.lb
         ub += variable.ub
      }

      val sum = if (name != null) m.intVar(name, lb, ub) else m.intVar(lb, ub)
      m.sum(vars, "=", sum).post()
      val sumDecomposition = decomposition.getOrPut(sum.id) { mutableListOf() }
      for (variable in vars) {
         if (variable !in sumDecomposition) {
            sumDecomposition += variable
         }
      }

      return sum
   }

}