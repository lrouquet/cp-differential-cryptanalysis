package fr.limos.decrypt.apps.rijndael.step1.optimizeiterative.sat

import com.github.rloic.phd.core.cryptography.rijndael.Rijndael.maxRounds
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.optimization.Configuration
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.optimization.PlainTextSolutionPresenter
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.optimization.Solution
import com.github.rloic.phd.core.error.panic
import com.github.rloic.phd.core.mzn.MznModelBuilder
import com.github.rloic.phd.core.mzn.MznVariable
import com.github.rloic.phd.core.mzn.OptimizationSearch
import com.github.rloic.phd.core.mzn.PartialMznModel
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import fr.limos.decrypt.apps.rijndael.step1.helpers.KeySchedule
import fr.limos.decrypt.apps.rijndael.step1.common.sat.OptimizationSolutionParser
import fr.limos.decrypt.configuration.Libraries
import javaextensions.io.dir
import javaextensions.io.div

fun main(_args_: Array<String>) {
    val args = parseArgs(_args_)

    var config = Configuration(
        args["MinNr"]?.toInt() ?: 3,
        args.expectArgument("TextSize").toInt(),
        args.expectArgument("KeySize").toInt(),
    )

    val minizinc = MiniZincBinary(args["Minizinc"] ?: Libraries.DEFAULT.minizinc)
    val picat = PicatBinary(
        args["Picat"] ?: Libraries.DEFAULT.picat,
        args["PicatFzn"] ?: Libraries.DEFAULT.picat_fzn,
        minizinc
    )

    val maxRounds = args["MaxNr"]?.toInt() ?: maxRounds[config.Nb - 4, config.Nk - 4]

    var minObjStep1: Int? = args["MinObjStep1"]?.toInt()
    if (minObjStep1 != null && minObjStep1 < 1) throw IllegalArgumentException("MinObjStep1 must be >= 1")

    val presenter = PlainTextSolutionPresenter(System.out)

    val sat = dir("sat")
    val tmpDir = dir("sat/tmp/minimize")

    var exitClause: ExitCause? = null
    val best = mutableListOf<Any?>()
    while (config.Nr <= maxRounds && exitClause == null) {
        println("Rijndael/Step1/MinimizeIter $config; MinObjStep1=$minObjStep1")

        if (minObjStep1 ?: 1 >= config.Nk * 6) {
            exitClause = ExitCause.TotalProbabilityBound
            break
        }

        val modelParts = listOf(PartialMznModel(sat / "Rijndael.mzn"), KeySchedule.generate(config, overwrite = true))
        val builder = MznModelBuilder.Optimization(modelParts, MznVariable("objStep1"), OptimizationSearch.Minimize)
        val mznModel = builder.build(tmpDir / "tmp-minimize-sat--${config.textSize}-${config.keySize}_${config.Nr}.mzn")
        if (minObjStep1 != null) {
            mznModel.value.appendText("constraint objStep1 >= $minObjStep1;\n")
        }

        val mznSolution = picat.optimize(
            mznModel, mapOf(
                "Nr" to config.Nr,
                "CIPHER_BITS" to config.textSize,
                "KEY_BITS" to config.keySize
            )
        )!!

        val parsedSolution = OptimizationSolutionParser(config).parse(mznSolution)
        presenter.present(parsedSolution)
        exitClause = checkProbability(parsedSolution)
        best += parsedSolution.objStep1
        minObjStep1 = parsedSolution.objStep1

        print("Min ObjStep1: ")
        println(minObjStep1)
        config = config.copy(Nr = config.Nr + 1)
    }

    if (config.Nr > maxRounds) {
        exitClause = ExitCause.MaxIterationReached
    }

    best += exitClause
    println(best.joinToString(","))
}

enum class ExitCause {
    MaxIterationReached,
    TotalProbabilityBound,
    TextProbabilityBound
}

private fun checkProbability(
    step1Sol: Solution,
): ExitCause? {
    val Nk = step1Sol.config.Nk
    val Nb = step1Sol.config.Nb
    val Nr = step1Sol.config.Nr
    val NRows = 4
    val objStep1 = step1Sol.objStep1
    val textSize = step1Sol.config.textSize
    val keySize = step1Sol.config.keySize
    var nbSBoxesInCipher = 0
    var nbSBoxesInKeySchedule = 0

    for (i in 0 until Nr) {
        for (j in 0 until NRows) {
            for (k in 0 until Nb) {
                if (step1Sol.ΔX[i, j, k] == 1) {
                    nbSBoxesInCipher += 1
                }
            }
        }
    }

    for (k in Nk - 1 until Nb * (Nr + 1)) {
        if (step1Sol.config.isSbColumn(k)) {
            for (j in 0 until NRows) {
                if (step1Sol.ΔWK[j, k] == 1) {
                    nbSBoxesInKeySchedule += 1
                }
            }
        }
    }

    if (nbSBoxesInCipher + nbSBoxesInKeySchedule != objStep1) {
        panic("Counted number of S-Boxes differs from expected, counted ${nbSBoxesInCipher + nbSBoxesInKeySchedule}, expected: $objStep1")
    }

    if (nbSBoxesInCipher * 6 > textSize) {
        System.err.println("objStep1 ($objStep1) * 6 > text_size ($textSize)")
        return ExitCause.TextProbabilityBound
    }
    if ((nbSBoxesInCipher + nbSBoxesInKeySchedule) * 6 > keySize) {
        return ExitCause.TotalProbabilityBound
    }

    return null
}