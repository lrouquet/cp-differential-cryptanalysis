package fr.limos.decrypt.apps.rijndael.step1.common.sat

import com.github.rloic.phd.core.mzn.Assignment
import com.github.rloic.phd.core.mzn.MznSolution

class PartialAssignmentExtractor(private val parser: EnumerationSolutionParser) {

    fun extract(mznSolution: MznSolution): Assignment.Partial {
        val solution = parser.parse(mznSolution)

        val xValues = solution.ΔX.deepFlatten().joinToString(",")
        val wkValues = solution.ΔWK.deepFlatten().joinToString(",")

        return Assignment.Partial(buildString {
            appendLine("array[ROUNDS, ROWS, CIPHER_COLS] of var 0..1: X_%SOL% = array3d(ROUNDS, ROWS, CIPHER_COLS, [$xValues]);")
            appendLine("array[ROWS, KEYSCHEDULE_COLS] of var 0..1: WK_%SOL% = array2d(ROWS, KEYSCHEDULE_COLS, [$wkValues]);")
        })
    }

}