@file:Suppress("NonAsciiCharacters")

package fr.limos.decrypt.apps.rijndael.step1.enumerate.cp

import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Configuration
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs

fun main(_args_: Array<String>) {

    val args = parseArgs(_args_)

    val config = Configuration(
        args.expectArgument("Nr").toInt(),
        args.expectArgument("TextSize").toInt(),
        args.expectArgument("KeySize").toInt(),
        args.expectArgument("ObjStep1").toInt()
    )

    val model = Enumerate(config)
    val solver = model.m.solver

    while (solver.solve()) {
        solver.printShortStatistics()
    }
    solver.printShortStatistics()
}