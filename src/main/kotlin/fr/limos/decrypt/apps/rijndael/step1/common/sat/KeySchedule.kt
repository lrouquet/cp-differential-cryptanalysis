package fr.limos.decrypt.apps.rijndael.step1.helpers

import com.github.rloic.phd.core.mzn.PartialMznModel
import fr.limos.decrypt.apps.rijndael.step1.helpers.ByteMod.Normal
import fr.limos.decrypt.apps.rijndael.step1.helpers.ByteMod.Sbox
import fr.limos.decrypt.utils.div
import javaextensions.io.dir
import kotlin.math.min
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Configuration as EnumConfiguration
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.optimization.Configuration as OptConfiguration

typealias Equation = Set<KeyPosition>

enum class ByteMod { Normal, Sbox }

data class KeyPosition(
    val row: Int,
    val column: Int,
    val mod: ByteMod = Normal
) {

    val isBox: Boolean get() = mod == Sbox

}

object KeySchedule {
    fun generate(config: OptConfiguration, overwrite: Boolean = false) = generate(config.textSize, config.keySize, config.Nr, overwrite)
    fun generate(config: EnumConfiguration, overwrite: Boolean = false) = generate(config.textSize, config.keySize, config.Nr, overwrite)

    fun generate(cipherBits: Int, keyBits: Int, Nr: Int, overwrite: Boolean = false): PartialMznModel {

        val target = dir("sat/xors") / "${cipherBits}_${keyBits}_${Nr}.mzn"
        val model = PartialMznModel(target)
        if (target.exists() && !overwrite) return model

        val Nb = cipherBits / 32
        val Nk = keyBits / 32

        val equations = mutableSetOf<Equation>()

        for (k in Nk until Nb * (Nr + 1)) {
            if (k % Nk == 0) {
                for (i in 0..3) {
                    equations += setOf(KeyPosition(i, k), KeyPosition(i, k - Nk), KeyPosition((i + 1) % 4, k - 1, Sbox))
                }
            } else if (Nk > 6 && k % Nk == 4) {
                for (i in 0..3) {
                    equations += setOf(KeyPosition(i, k), KeyPosition(i, k - Nk), KeyPosition(i, k - 1, Sbox))
                }
            } else {
                for (i in 0..3) {
                    equations += setOf(KeyPosition(i, k), KeyPosition(i, k - Nk), KeyPosition(i, k - 1))
                }
            }
        }

        val xorEq = combine(equations, equations) + equations

        target.apply {
            if (exists()) { delete() }
            createNewFile()
        }

        target.bufferedWriter().use { writer ->
            for (xor in xorEq) {

                if (xor.size == 3) {
                    val (A, B, C) = xor.toList()

                    if (A.isBox || B.isBox || C.isBox) writer.write("constraint WK[${A.row}, ${A.column}] + WK[${B.row}, ${B.column}] + WK[${C.row}, ${C.column}] != 1;\n")
                    if (!A.isBox && !B.isBox) writer.write("constraint diffWK[${A.column}, ${A.row}, ${B.column}] = WK[${C.row}, ${C.column}];\n")
                    if (!B.isBox && !C.isBox) writer.write("constraint diffWK[${B.column}, ${B.row}, ${C.column}] = WK[${A.row}, ${A.column}];\n")
                    if (!C.isBox && !A.isBox) writer.write("constraint diffWK[${C.column}, ${C.row}, ${A.column}] = WK[${B.row}, ${B.column}];\n")
                } else {
                    val (A, B, C, D) = xor.toList()

                    if (!A.isBox && !B.isBox && !C.isBox && !D.isBox) {
                        writer.write("constraint diffWK[${A.column}, ${A.row}, ${B.column}] = diffWK[${C.column}, ${C.row}, ${D.column}];\n");
                        writer.write("constraint diffWK[${A.column}, ${A.row}, ${C.column}] = diffWK[${B.column}, ${B.row}, ${D.column}];\n");
                        writer.write("constraint diffWK[${A.column}, ${A.row}, ${D.column}] = diffWK[${B.column}, ${B.row}, ${C.column}];\n");
                    } else {
                        writer.write("constraint WK[${A.row}, ${A.column}] + WK[${B.row}, ${B.column}] + WK[${C.row}, ${C.column}] + WK[${D.row}, ${D.column}] != 1;\n")
                    }
                }

            }

            for (xor in xorEq) {
                val lXor = xor.toList()
                if (xor.size == 3) {
                    val (j, k1) = lXor[0]
                    val (_, k2) = lXor[1]
                    writer.write("constraint diffWK[$k1, $j, $k2] + WK[$j, $k1] + WK[$j, $k2] != 1;\n")
                } else if (xor.size == 4) {
                    val (j, k1) = lXor[0]
                    val (_, k2) = lXor[1]
                    val (_, k3) = lXor[2]
                    writer.write("constraint diffWK[$k1, $j, $k2] + diffWK[$k2, $j, $k3] + diffWK[$k1, $j, $k3] != 1;\n")
                }
            }
        }
        return model
    }
}

fun combine(lhs: Set<Equation>, rhs: Set<Equation>): Set<Equation> {
    if (lhs.isEmpty()) return emptySet()

    val newEquationSet = mutableSetOf<Equation>()
    for (equation1 in lhs) {
        for (equation2 in rhs) {
            if (equation1 != equation2) {
                val result = equation1 xor equation2
                if (result.size < min(equation1.size + equation2.size, 5) && result !in rhs) {
                    newEquationSet += result
                }
            }
        }
    }

    return newEquationSet union combine(newEquationSet, newEquationSet union rhs)
}

// Return a new equation such as res = this \oplus other
infix fun Equation.xor(other: Equation): Equation {
    val result = mutableSetOf<KeyPosition>()

    result += this.filter { it !in other }
    result += other.filter { it !in this }

    return result
}

// Return a new set such as res = this \cup other
infix fun <T> MutableSet<T>.union(other: Set<T>): MutableSet<T> {
    val copy = toMutableSet()
    copy.addAll(other)
    return copy
}