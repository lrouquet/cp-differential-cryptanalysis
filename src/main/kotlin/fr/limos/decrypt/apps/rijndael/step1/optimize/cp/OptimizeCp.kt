package fr.limos.decrypt.apps.rijndael.step1.optimize.cp

import com.github.rloic.phd.core.arrays.Matrix
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.optimization.Configuration
import fr.limos.decrypt.apps.rijndael.step1.common.cp.Step1
import fr.limos.decrypt.strategy.DomOverWDegWithDecomposition
import org.chocosolver.solver.Model
import org.chocosolver.solver.search.strategy.Search
import org.chocosolver.solver.search.strategy.Search.lastConflict
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDegRef
import org.chocosolver.solver.variables.IntVar

@Suppress("NonAsciiCharacters")
class OptimizeCp(
    config: Configuration,
) {

    private val delegate = Step1(config.Nr, config.Nb, config.Nk, config::isSbColumn)

    val sboxes get() = delegate.sboxes
    val m get() = delegate.m

    val objStep1: IntVar

    init {
        objStep1 = m.intVar(0, sboxes.size)
        m.sum(sboxes.toTypedArray(), "=", objStep1).post()
        m.setObjective(Model.MINIMIZE, objStep1)

        val solver = m.solver

        val X0 = delegate.ΔX[0].deepFlatten()
        val K0 = Matrix(4, config.Nk) { j, ik -> delegate.ΔWK[j, ik] }.deepFlatten()

        m.sum((X0 + K0).toTypedArray(), ">", 0).post()

        m.sum(delegate.nbActives, "=", objStep1).post()
        m.sum(delegate.sboxColumns, "=", objStep1).post()
        m.sum(delegate.sboxes.toTypedArray(), "=", objStep1).post()

        val otherVars = m.retrieveIntVars(true).toMutableList()

        otherVars -= delegate.nbActives
        otherVars -= delegate.sboxColumns
        otherVars -= delegate.sboxes

        solver.setSearch(
            DomOverWDegWithDecomposition((delegate.nbActives + delegate.sboxColumns), delegate.decomposition, 0, IntDomainMin()),
            DomOverWDegRef(delegate.sboxes.toTypedArray(), 0, IntDomainMin()),
            Search.inputOrderLBSearch(*otherVars.toTypedArray())
        )
        solver.setSearch(lastConflict(solver.getSearch(), 16))

    }

}