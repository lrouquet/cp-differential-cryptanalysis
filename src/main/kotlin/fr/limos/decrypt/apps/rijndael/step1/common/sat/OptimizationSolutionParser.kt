package fr.limos.decrypt.apps.rijndael.step1.common.sat

import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.optimization.Configuration
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.optimization.Solution
import com.github.rloic.phd.core.mzn.MznSolution
import com.github.rloic.phd.core.mzn.MznSolutionParser
import javaextensions.util.reshape

class OptimizationSolutionParser(private val config: Configuration): MznSolutionParser<Solution> {

    private fun parseObjStep1(line: String): Int {
        return line
            .substringAfter(" = ")
            .substringBefore(';')
            .toInt()
    }

    private fun parseBlockSequence(line: String): IntArray {
        return line.substringAfter('[')
            .substringBefore(']')
            .split(",")
            .map { if (it == "1" || it == "true") 1 else 0 }
            .toIntArray()
    }

    override fun parse(serialized: MznSolution): Solution {

        val lines = serialized.content.split('\n')
        fun getVariableLine(variableName: String) = lines.first { it.startsWith(variableName) }

        val objStep1 = parseObjStep1(getVariableLine("objStep1"))
        val ΔX = parseBlockSequence(getVariableLine("X"))
            .reshape(config.Nr, 4, config.Nb)
        val ΔWK = parseBlockSequence(getVariableLine("WK"))
            .reshape(4, (config.Nr + 1) * config.Nb)
        return Solution(config, objStep1, ΔX, ΔWK)

    }
}