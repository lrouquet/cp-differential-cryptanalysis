package fr.limos.decrypt.apps.rijndael.step2

import com.github.rloic.phd.core.arrays.IntMatrix
import com.github.rloic.phd.core.arrays.IntTensor3
import fr.limos.decrypt.ciphers.rijndael.Rijndael

fun displayConcreteSolution(
    Nr: Int,
    rijndael: Rijndael,
    WK: IntMatrix,
    X: IntTensor3,
    SX: IntTensor3,
    Y: IntTensor3,
    Z: IntTensor3,
    indent: Int = 2
) {
    val Nb = rijndael.textCols();
    val leftSpaces = buildString { repeat(indent) { append(' ') } }

    fun Int.toHexString(pad: Int = 0): String =
        "0x" + if (pad == 0) {
            Integer.toBinaryString(this).toUpperCase()
        } else {
            Integer.toHexString(this).padStart(pad, '0').toUpperCase()
        }

    fun display(name: String, block: IntMatrix) {
        print(leftSpaces)
        println(name)
        for (j in 0 until 4) {
            print(leftSpaces)
            for (k in 0 until Nb) {
                print(block[j, k].toHexString(2))
                print(" ")
            }
            println()
        }
        println()
    }

    fun displayPair(leftName: String, leftBlock: IntMatrix, rightName: String, rightBlock: IntMatrix) {
        print(leftSpaces)
        print(leftName.padEnd(Nb * 5))
        print('\t')
        println(rightName)
        for (j in 0 until 4) {
            print(leftSpaces)
            for (k in 0 until Nb) {
                print(leftBlock[j, k].toHexString(2))
                print(" ")
            }
            print("\t")
            for (k in 0 until Nb) {
                print(rightBlock[j, k].toHexString(2))
                print(" ")
            }
            println()
        }
        println()
    }

    val plainText = IntMatrix(4, Nb) { j, k -> X[0, j, k] xor WK[j, k] }
    val K: IntTensor3 = IntTensor3(Nr, 4, Nb) { i, j, k -> WK[j, i * Nb + k] }

    displayPair("δPlainText:", plainText, "δK[0]:", K[0])

    for (i in 0 until Nr - 1) {
        display("δX[$i]:", X[i])
        display("δSX[$i]:", SX[i])
        display("δY[$i]:", Y[i])
        displayPair("δZ[$i]:", Z[i], "δK[${i + 1}]:", K[i + 1])
    }

    display("δX[${Nr - 1}]:", X[Nr - 1])
    display("δSX[${Nr - 1}]:", SX[Nr - 1])
    display("δY[${Nr - 1}]:", Y[Nr - 1])

}