package fr.limos.decrypt.apps.rijndael.step1.enumerate.cp

import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Configuration
import fr.limos.decrypt.apps.rijndael.step1.MinRoundsConfig
import fr.limos.decrypt.apps.rijndael.step1.common.cp.Step1
import fr.limos.decrypt.strategy.DomOverWDegWithDecomposition
import fr.limos.decrypt.strategy.EnumFilter
import org.chocosolver.solver.Model
import org.chocosolver.solver.search.strategy.Search.inputOrderLBSearch
import org.chocosolver.solver.search.strategy.Search.lastConflict
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDegRef
import org.chocosolver.solver.variables.IntVar
import java.io.File

@Suppress("NonAsciiCharacters")
class Enumerate(
   config: Configuration,
) {

   val objStep1 = config.objStep1
   val Nr = config.Nr
   val Nb = config.Nb
   val Nk = config.Nk

   private val delegate = Step1(config.Nr, config.Nb, config.Nk, config::isSbColumn)

   val sboxes get() = delegate.sboxes
   val m get() = delegate.m

   init {
      val solver = m.solver

      solver.plugMonitor(EnumFilter(m, delegate.sboxes.toTypedArray(), objStep1))

      m.sum(delegate.nbActives, "=", objStep1).post()
      m.sum(delegate.sboxColumns, "=", objStep1).post()
      m.sum(delegate.sboxes, "=", objStep1).post()

      val MIN_OBJ_STEP1 = MinRoundsConfig(File(javaClass.classLoader.getResource("min_rounds.txt")!!.toURI()))

      val cumulSum = arrayOfNulls<IntVar>(Nr + 1)
      cumulSum[0] = delegate.nbActives[0]
      delegate.decomposition[cumulSum[0]!!.id] = delegate.decomposition[delegate.nbActives[0].id]
      for (i in 1..Nr) {
         val name = cumulSum[i - 1]!!.name + " + " + delegate.nbActives[i].name
         cumulSum[i] = m.intVar(name, MIN_OBJ_STEP1[Nb, Nk, i], config.objStep1)
         delegate.decomposition[cumulSum[i]!!.id] =
            (delegate.decomposition[cumulSum[i - 1]!!.id] + delegate.decomposition[delegate.nbActives[i].id]).toMutableList()
         m.arithm(cumulSum[i - 1], "+", delegate.nbActives[i], "=", cumulSum[i]).post()
      }

      val complementSum = arrayOfNulls<IntVar>(Nr)
      complementSum[Nr - 1] = delegate.nbActives[Nr]
      delegate.decomposition[complementSum[Nr - 1]!!.id] = delegate.decomposition[delegate.nbActives[Nr].id]
      for (i in Nr - 2 downTo 0) {
         val name: String = delegate.nbActives[i + 1].name + " + " + complementSum[i + 1]!!.name
         complementSum[i] = m.intVar(name, 0, config.objStep1 - MIN_OBJ_STEP1[Nb, Nk, i])
         delegate.decomposition[cumulSum[i]!!.id] =
            (delegate.decomposition[cumulSum[i + 1]!!.id] + delegate.decomposition[delegate.nbActives[i + 1].id]).toMutableList()
         m.arithm(delegate.nbActives[i + 1], "+", complementSum[i + 1], "=", complementSum[i]).post()
      }

      for (i in 0 until Nr) {
         m.arithm(cumulSum[i], "+", complementSum[i], "=", config.objStep1).post()
      }

      val otherVars = m.retrieveIntVars(true).toMutableList()

      otherVars -= delegate.intVarToIgnore
      otherVars -= delegate.nbActives
      otherVars -= delegate.sboxColumns
      otherVars -= delegate.sboxes
      otherVars -= cumulSum
      otherVars -= complementSum

        solver.setSearch(
            DomOverWDegWithDecomposition((delegate.nbActives + delegate.sboxColumns), delegate.decomposition, 0, IntDomainMin()),
            DomOverWDegRef(delegate.sboxes.toTypedArray(), 0, IntDomainMin()),
            inputOrderLBSearch(*otherVars.toTypedArray())
        )
        solver.setSearch(lastConflict(solver.getSearch(), 16))
    }

   private fun Model.sum(vars: List<IntVar>, op: String, value: Int) = sum(vars.toTypedArray(), op, value)
   private fun Model.sum(vars: List<IntVar>, op: String, value: IntVar) = sum(vars.toTypedArray(), op, value)

}