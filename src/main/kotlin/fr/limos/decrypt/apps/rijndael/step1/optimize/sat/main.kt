package fr.limos.decrypt.apps.rijndael.step1.optimize.sat

import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.optimization.Configuration
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.optimization.PlainTextSolutionPresenter
import com.github.rloic.phd.core.mzn.MznModelBuilder
import com.github.rloic.phd.core.mzn.MznVariable
import com.github.rloic.phd.core.mzn.OptimizationSearch
import com.github.rloic.phd.core.mzn.PartialMznModel
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import fr.limos.decrypt.apps.rijndael.step1.helpers.KeySchedule
import fr.limos.decrypt.apps.rijndael.step1.common.sat.OptimizationSolutionParser
import fr.limos.decrypt.configuration.Libraries
import fr.limos.decrypt.utils.div
import java.io.File
import javaextensions.io.dir

fun main(_args_: Array<String>) {
    val sat = File("sat")
    val args = parseArgs(_args_)

    val config = Configuration(
        args.expectArgument("Nr").toInt(),
        args.expectArgument("TextSize").toInt(),
        args.expectArgument("KeySize").toInt()
    )

    val minizinc = MiniZincBinary(args["MiniZinc"] ?: Libraries.DEFAULT.minizinc)
    val picat = PicatBinary(
        args["Picat"] ?: Libraries.DEFAULT.picat,
        args["PicatFzn"] ?: Libraries.DEFAULT.picat_fzn,
        minizinc
    )

    println("Rijndael/Step1/MinimizeSat $config")
    val solutionParser = OptimizationSolutionParser(config)

    val modelParts = listOf(PartialMznModel(sat / "Rijndael.mzn"), KeySchedule.generate(config, overwrite = true))
    val objectiveVar = MznVariable("objStep1")

    val builder = MznModelBuilder.Optimization(modelParts, objectiveVar, OptimizationSearch.Minimize)
    val mznModel = builder.build(dir("sat/tmp/minimize") / "tmp-minimize-sat--${config.textSize}-${config.keySize}_${config.Nr}.mzn")


    val mznSolution = picat.optimize(
        mznModel, mapOf(
            "Nr" to config.Nr,
            "CIPHER_BITS" to config.textSize,
            "KEY_BITS" to config.keySize
        )
    )

    if (mznSolution != null) {
        val solution = solutionParser.parse(mznSolution)
        PlainTextSolutionPresenter(System.out).present(solution)
        println("ObjStep1")
        println(solution.objStep1)
    } else {
        println("ObjStep1")
        println("UNSAT")
    }
}