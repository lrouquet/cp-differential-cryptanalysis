package fr.limos.decrypt.apps.rijndael.step1.enumerate.sat

import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Configuration
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.PlainTextSolutionPresenter
import com.github.rloic.phd.core.mzn.MznModelBuilder
import com.github.rloic.phd.core.mzn.PartialMznModel
import com.github.rloic.phd.core.mzn.SearchStrategy
import com.github.rloic.phd.core.mzn.ValueSelector
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import fr.limos.decrypt.apps.rijndael.step1.helpers.KeySchedule
import fr.limos.decrypt.apps.rijndael.step1.common.sat.EnumerationSolutionParser
import fr.limos.decrypt.apps.rijndael.step1.common.sat.PartialAssignmentExtractor
import fr.limos.decrypt.configuration.Libraries
import fr.limos.decrypt.utils.div
import javaextensions.io.dir
import java.io.File

fun main(_args_: Array<String>) {
    val args = parseArgs(_args_)

    val config = Configuration(
        args.expectArgument("Nr").toInt(),
        args.expectArgument("TextSize").toInt(),
        args.expectArgument("KeySize").toInt(),
        args.expectArgument("ObjStep1").toInt()
    )

    val minizinc = MiniZincBinary(args["MiniZinc"] ?: Libraries.DEFAULT.minizinc)
    val picat = PicatBinary(
        args["Picat"] ?: Libraries.DEFAULT.picat,
        args["PicatFzn"] ?: Libraries.DEFAULT.picat_fzn,
        minizinc
    )

    println("Rijndael/Step1/EnumerateSat $config")
    val sat = File("sat")

    val solutionParser = EnumerationSolutionParser(config)
    val assignmentExtractor = PartialAssignmentExtractor(solutionParser)

    val modelParts = listOf(PartialMznModel(sat / "Rijndael.mzn"), KeySchedule.generate(config, overwrite = true))
    val decisionVars = PartialMznModel(sat / "DecisionVars.mzn")
    val forbidSolution = PartialMznModel(sat / "ForbidSolution.mzn")

    val builder = MznModelBuilder.PartialAssignment(
        modelParts, decisionVars, SearchStrategy.Smallest, ValueSelector.InDomainMin, forbidSolution,
        assignmentExtractor::extract
    )

    val mznModel =
        builder.build(dir("sat/tmp/enumerate") / "tmp-enumerate-sat--${config.textSize}-${config.keySize}_${config.Nr}.mzn")

    val data = mapOf(
        "Nr" to config.Nr,
        "CIPHER_BITS" to config.textSize,
        "KEY_BITS" to config.keySize,
        "objStep1" to config.objStep1
    )

    val parser = EnumerationSolutionParser(config)
    val presenter = PlainTextSolutionPresenter(System.out)
    var nbSols = 0
    for (mznSolution in picat.enumerate(mznModel, data)) {
        presenter.present(parser.parse(mznSolution))
        nbSols += 1
    }
    println("== END ==")
    println("#Solutions")
    println(nbSols)
}