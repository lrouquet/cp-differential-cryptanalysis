package fr.limos.decrypt.apps.rijndael.step2.sat

import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Configuration
import com.github.rloic.phd.core.files.csv.CSV
import com.github.rloic.phd.core.mzn.MznModelBuilder
import com.github.rloic.phd.core.mzn.PartialMznModel
import com.github.rloic.phd.core.mzn.SearchStrategy
import com.github.rloic.phd.core.mzn.ValueSelector
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.core.utils.parseArgs
import com.github.rloic.phd.infra.MiniZincBinary
import com.github.rloic.phd.infra.PicatBinary
import fr.limos.decrypt.apps.rijndael.step1.helpers.KeySchedule
import fr.limos.decrypt.apps.rijndael.step1.common.sat.EnumerationSolutionParser
import fr.limos.decrypt.apps.rijndael.step1.common.sat.PartialAssignmentExtractor
import fr.limos.decrypt.configuration.Libraries
import fr.limos.decrypt.utils.div
import javaextensions.io.dir
import java.io.File

fun main(_args_: Array<String>) {

    val args = parseArgs(_args_)

    val nr = args.expectArgument("Nr").toInt()
    val textSize = args.expectArgument("TextSize").toInt()
    val keySize = args.expectArgument("KeySize").toInt()
    val objStep1 = args.expectArgument("ObjStep1").toInt()

    var config = Configuration(nr, textSize, keySize, objStep1)

    val minizinc = MiniZincBinary(args["MiniZinc"] ?: Libraries.DEFAULT.minizinc)
    val picat = PicatBinary(
        args["Picat"] ?: Libraries.DEFAULT.picat,
        args["PicatFzn"] ?: Libraries.DEFAULT.picat_fzn,
        minizinc
    )

    val sat = File("sat")

    val solutionParser = EnumerationSolutionParser(config)
    val assignmentExtractor = PartialAssignmentExtractor(solutionParser)

    val modelParts = listOf(PartialMznModel(sat / "Rijndael.mzn"), KeySchedule.generate(config, overwrite = true))
    val decisionVars = PartialMznModel(sat / "DecisionVars.mzn")
    val forbidSolution = PartialMznModel(sat / "ForbidSolution.mzn")

    val builder = MznModelBuilder.PartialAssignment(
        modelParts, decisionVars, SearchStrategy.Smallest, ValueSelector.InDomainMin, forbidSolution,
        assignmentExtractor::extract
    )

    val target = dir("sat/tmp/enumerate") / "tmp-enumerate-sat--${config.textSize}-${config.keySize}_${config.Nr}.mzn"
    val mznModel = builder.build(target)

    var UB = config.objStep1 * -6
    var LB: Int? = null

    var iter = picat.enumerate(mznModel, mapOf(
        "Nr" to config.Nr,
        "CIPHER_BITS" to config.textSize,
        "KEY_BITS" to config.keySize,
        "objStep1" to config.objStep1
    ))

    var objStep1OfFirstSolution: Int? = null
    var objStep1OfBestSolution: Int? = null
    var maxObjStep1: Int? = null

    while ((LB ?: -keySize) < UB) {
        maxObjStep1 = config.objStep1
        println("Solving step1 for objStep1 = $maxObjStep1")
        if (iter.hasNext()) {
            val s1MznSolution = iter.next()
            val s1 = EnumerationSolutionParser(config).parse(s1MznSolution)

            val step2Opt = OptimizeForSatKt(s1, LB ?: -keySize)
            val step2Solver = step2Opt.m.solver

            while (step2Solver.solve()) {
                if (objStep1OfFirstSolution == null) {
                    objStep1OfFirstSolution = config.objStep1
                }

                objStep1OfBestSolution = config.objStep1
                LB = step2Opt.probSum.value
            }
        } else {
            config = config.copy(objStep1 = config.objStep1 + 1)
            UB = config.objStep1 * -6
            iter = picat.enumerate(mznModel, mapOf(
                "Nr" to config.Nr,
                "CIPHER_BITS" to config.textSize,
                "KEY_BITS" to config.keySize,
                "objStep1" to config.objStep1
            ))
        }
    }

    println("===")

    println(
        CSV.formatLine(
            "obj_step1",
            "obj_step1 of first solution",
            "obj_step1 of best solution",
            "max obj_step1",
            "best_probability",
        ).content
    )

    println(
        CSV.formatLine(
            objStep1,
            objStep1OfFirstSolution,
            objStep1OfBestSolution,
            maxObjStep1,
            if (LB != null) "$2^{$LB}$" else "$0$",
        ).content
    )

}

