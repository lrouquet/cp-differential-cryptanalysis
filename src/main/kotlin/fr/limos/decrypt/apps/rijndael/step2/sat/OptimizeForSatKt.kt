package fr.limos.decrypt.apps.rijndael.step2.sat

import com.github.rloic.phd.core.arrays.IntMatrix
import com.github.rloic.phd.core.arrays.Matrix
import com.github.rloic.phd.core.arrays.Tensor3
import com.github.rloic.phd.core.arrays.matrixOfNulls
import com.github.rloic.phd.core.cryptography.rijndael.Rijndael.OPTIMAL_INV_SBOX_TRANSITION
import com.github.rloic.phd.core.cryptography.rijndael.Rijndael.OPTIMAL_SBOX_TRANSITION
import com.github.rloic.phd.core.cryptography.rijndael.Rijndael.shifts
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step2.optimization.LatexCharacteristicDistinguisherPresenter
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step2.optimization.PlainTextSolutionPresenter
import fr.limos.decrypt.ciphers.rijndael.Rijndael
import fr.limos.decrypt.models.CryptoModelStep2
import fr.limos.decrypt.solver.search.strategy.selectors.values.IntDomainBestSbox
import fr.limos.decrypt.utils.IntPtr
import fr.limos.decrypt.utils.ValuesExtractor
import javaextensions.io.dir
import javaextensions.io.div
import org.chocosolver.solver.Model
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution
import org.chocosolver.solver.search.strategy.Search
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDegRef
import org.chocosolver.solver.variables.IntVar
import java.lang.Integer.max
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Solution as Step1Solution
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step2.optimization.Solution as Step2Solution

@Suppress("NonAsciiCharacters")
class OptimizeForSatKt(
    val step1Sol: Step1Solution,
    val LB: Int,
) {

    private val Nr = step1Sol.config.Nr
    private val Nb = step1Sol.config.Nb
    private val Nk = step1Sol.config.Nk
    private val NRows = 4

    val m = CryptoModelStep2(
        "Rijndael/Step2/OptimizeSat",
        Rijndael(step1Sol.config),
        Nr
    )

    val probabilities: List<IntVar>

    val δX: Tensor3<IntVar>
    val pX: Tensor3<IntVar>
    val δSX: Tensor3<IntVar>
    val δY: Tensor3<IntVar>
    val δZ: Tensor3<IntVar>

    val δWK: Matrix<IntVar>
    val pWK: Matrix<IntVar?>

    val probSum: IntVar

    init {
        pWK = matrixOfNulls(NRows, (Nr + 1) * Nb)
        δWK = expandedKey(step1Sol.ΔWK, pWK)

        δX = Tensor3(Nr, NRows, Nb) { i, j, k ->
            if (step1Sol.ΔX[i, j, k] == 1) m.intVar(1, 255)
            else m.ZERO
        }
        pX = Tensor3(Nr, NRows, Nb) { i, j, k ->
            if (step1Sol.ΔX[i, j, k] == 1) m.intVar(-7, -6)
            else m.ZERO
        }
        δSX = Tensor3(Nr, NRows, Nb) { i, j, k -> m.subCell(δX[i, j, k], pX[i, j, k]) }
        δY = Tensor3(Nr, NRows, Nb) { i, j, k -> δSX[i, j, (k + shifts[Nb - 4, j]) % Nb] }
        δZ = Tensor3(Nr - 1) { i -> Rijndael.mixColumn(m, δY[i]) }
        for (i in 0 until Nr - 1) {
            for (j in 0 until NRows) {
                for (k in 0 until Nb) {
                    m.postXor(δX[i + 1, j, k], δZ[i, j, k], δWK[j, (i + 1) * Nb + k])
                }
            }
        }

        probabilities = pX.deepFlatten().filter { it != m.ZERO } + pWK.deepFlatten().filterNotNull().filter { it != m.ZERO }

        probSum = m.intVar(max(-7 * step1Sol.config.objStep1, LB), -6 * step1Sol.config.objStep1)
        m.sum(probabilities.toTypedArray(), "=", probSum).post()
        m.delegateModel.setObjective(Model.MAXIMIZE, probSum)

        val order = mutableListOf<IntVar>()
        for (i in 0 until Nr + 1) {
            for (k in 0 until Nb) {
                for (j in 0 until NRows) {
                    if (i < Nr) order += δY[i, j, k]
                    if (step1Sol.config.isSbColumn(i * Nb + k)) {
                        order += δWK[j, i * Nb + k]
                    }
                }
            }
        }

        for (variable in m.delegateModel.retrieveIntVars(true)) {
            if (variable !in order) {
                order += variable
            }
        }

        val s = m.solver
        s.setSearch(Search.lastConflict(
            DomOverWDegRef(order.toTypedArray(), 0L, IntDomainBestSbox(
                OPTIMAL_SBOX_TRANSITION,
                OPTIMAL_INV_SBOX_TRANSITION,
                m.sboxes,
                IntDomainMin()
            )),
        ))

        s.plugMonitor(object : IMonitorSolution {
            val presenter = PlainTextSolutionPresenter(System.out)
            override fun onSolution() {

                val solution = Step2Solution(
                    step1Sol.config,
                    probSum.value,
                    ValuesExtractor.values(δX),
                    ValuesExtractor.values(δSX),
                    ValuesExtractor.values(pX),
                    ValuesExtractor.values(δY),
                    ValuesExtractor.values(δZ),
                    ValuesExtractor.values(δWK),
                    ValuesExtractor.valuesOfNullable(pWK)
                )

                presenter.present(solution)
            }
        })

    }

    private fun expandedKey(ΔWK: IntMatrix, pWK: Matrix<IntVar?>): Matrix<IntVar> {
        val δWK = Matrix(NRows, (Nr + 1) * Nb) { i, ik ->
            if (step1Sol.config.isSbColumn(ik)) {
                if (ΔWK[i, ik] == 1) {
                    pWK[i, ik] = m.intVar(-7, -6)
                    m.intVar(1, m.maxValue)
                } else {
                    pWK[i, ik] = m.ZERO
                    m.ZERO
                }
            } else {
                pWK[i, ik] = null
                m.byteVar()
            }
        }

        for (ik in Nk until Nb * (Nr + 1)) {
            if (ik % Nk == 0) {
                for (j in 0 until NRows) {
                    m.postXor(
                        δWK[j, ik],
                        δWK[j, ik - Nk],
                        m.subCell(δWK[(j + 1) % NRows, ik - 1], pWK[(j + 1) % NRows, ik - 1]!!)
                    )
                }
            } else if (Nk > 6 && ik % Nk == 4) {
                for (j in 0 until NRows) {
                    m.postXor(δWK[j, ik], δWK[j, ik - Nk], m.subCell(δWK[j, ik - 1], pWK[j, ik - 1]!!))
                }
            } else {
                for (j in 0 until NRows) {
                    m.postXor(δWK[j, ik], δWK[j, ik - Nk], δWK[j, ik - 1])
                }
            }
        }
        return δWK
    }

}