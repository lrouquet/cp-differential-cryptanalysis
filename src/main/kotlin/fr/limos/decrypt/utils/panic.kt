package fr.limos.decrypt.utils

fun panic(msg: String): Nothing = throw Exception(msg)