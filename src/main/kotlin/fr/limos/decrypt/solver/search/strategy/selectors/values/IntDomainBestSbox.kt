package fr.limos.decrypt.solver.search.strategy.selectors.values

import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap
import it.unimi.dsi.fastutil.ints.IntArraySet
import org.chocosolver.solver.search.strategy.selectors.values.IntValueSelector
import org.chocosolver.solver.variables.IntVar

class IntDomainBestSbox(
    val optimalSboxTransition: IntArray,
    val optimalInvSboxTransition: IntArray,
    sboxes: List<Triple<IntVar, IntVar, IntVar>>,
    val fallbackStrategy: IntValueSelector,
    val greaterIsBetter: Boolean = true
) : IntValueSelector {


    val sboxOf = Int2ObjectArrayMap<IntVar>()
    val invSboxOf = Int2ObjectArrayMap<IntVar>()
    val probabilities = IntArraySet()

    init {
        for ((inByte, outByte, p) in sboxes) {
            sboxOf[inByte.id] = outByte
            invSboxOf[outByte.id] = inByte
            probabilities.add(p.id)
        }
    }


    override fun selectValue(`var`: IntVar): Int {
        if (`var`.id in probabilities) {
            return if (greaterIsBetter) { `var`.ub } else { `var`.lb }
        }

        val sx = sboxOf[`var`.id]
        if (sx != null) {
            if (sx.isInstantiated) {
                val sxValue = sx.value
                val bestTransition = optimalInvSboxTransition[sxValue]
                if (bestTransition in `var`) {
                    return bestTransition
                }
            }
        }

        val x = invSboxOf[`var`.id]
        if (x != null) {
            if (x.isInstantiated) {
                val xValue = x.value
                val bestTransition = optimalSboxTransition[xValue]
                if (bestTransition in `var`) {
                    return bestTransition
                }
            }
        }

        return fallbackStrategy.selectValue(`var`)
    }
}