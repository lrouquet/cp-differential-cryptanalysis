package fr.limos.decrypt.configuration

import com.moandjiezana.toml.Toml
import java.io.File

data class Libraries(
    val picat: String,
    val picat_fzn: String,
    val minizinc: String
) {

    companion object {
        val DEFAULT: Libraries by lazy {
            Toml().read(File("config.toml"))
                .to(Libraries::class.java)
        }
    }

}