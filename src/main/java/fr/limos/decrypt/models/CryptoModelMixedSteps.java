package fr.limos.decrypt.models;

import fr.limos.decrypt.ciphering.HasSBoxes;
import fr.limos.decrypt.ciphering.IsBlockCipher;
import fr.limos.decrypt.ciphering.IsStep1Model;
import fr.limos.decrypt.ciphering.IsStep2Model;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;

public class CryptoModelMixedSteps<Cipher extends IsBlockCipher & HasSBoxes>
        implements IsStep1Model, IsStep2Model {

    public final CryptoModelStep1<Cipher> step1;
    public final CryptoModelStep2<Cipher> step2;
    public final Model delegateModel;
    public final Cipher delegateCipher;

    public CryptoModelMixedSteps(String name, Cipher cipher, int r, int sb) {
        step1 = new CryptoModelStep1<>(name, cipher, r, sb);
        step2 = new CryptoModelStep2<>(name, cipher, r).withModel(step1.delegateModel);
        delegateModel = step1.delegateModel;
        this.delegateCipher = cipher;
    }

    @Override
    public int getNbSboxes() {
        return step1.getNbSboxes();
    }

    public void reify(BoolVar abstraction, IntVar concrete) {
        delegateModel.ifOnlyIf(
                delegateModel.arithm(concrete, "=", 0),
                delegateModel.arithm(abstraction, "=", 0)
        );
    }

    public void reify(BoolVar[] abstractions, IntVar[] concretes) {
        for (int i = 0; i < Math.max(abstractions.length, concretes.length); i++) {
            reify(abstractions[i], concretes[i]);
        }
    }

    public void reify(BoolVar[][] abstractions, IntVar[][] concretes) {
        for (int i = 0; i < Math.max(abstractions.length, concretes.length); i++) {
            reify(abstractions[i], concretes[i]);
        }
    }

    public int getNbRounds() {
        assert step1.getNbRounds() == step2.getNbRounds();
        return step1.getNbRounds();
    }

    public int getLastRound() {
        assert step1.getLastRound() == step2.getLastRound();
        return step1.getLastRound();
    }

    public Solver getSolver() {
        return delegateModel.getSolver();
    }

}
