package fr.limos.decrypt.models;

import com.github.rloic.phd.core.arrays.Matrix;
import fr.limos.decrypt.ciphering.BlockDimensions;
import fr.limos.decrypt.ciphering.HasSBoxes;
import fr.limos.decrypt.ciphering.IsBlockCipher;
import fr.limos.decrypt.ciphering.IsStep2Model;
import fr.limos.decrypt.utils.BlockPosition;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import kotlin.Pair;
import kotlin.Triple;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.IntVar;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@SuppressWarnings("NonAsciiCharacters")
public class CryptoModelStep2<Cipher extends IsBlockCipher & HasSBoxes>
        extends CryptoModel<Cipher> implements IsStep2Model {

    public final IntVar ZERO = intVar(0);
    private final Tuples S_BOXES = delegateCipher.createRelationSBoxes();
    private final Tuples S_BOXES_NO_P = delegateCipher.createRelationSBoxes();
    private final Tuples XOR = createRelationXor();

    private final Int2ObjectMap<Tuples> GALOIS_FIELD_MULT = new Int2ObjectArrayMap<>();

    private CryptoModelStep2(Model m, Cipher cipher, int r) {
        super(m, cipher, r);
    }

    public CryptoModelStep2(String name, Cipher cipher, int r) {
        super(name, cipher, r);
    }

    @Override
    protected CryptoModelStep2<Cipher> withModel(Model m) {
        return new CryptoModelStep2<>(m, delegateCipher, r);
    }

    public IntVar byteVar() {
        return intVar(0, getMaxValue());
    }

    public IntVar subCell(IntVar δin, IntVar p) {
        return subCell(δin, p, DEFAULT_STRATEGY);
    }

    public IntVar subCell(IntVar δin, IntVar p, String strategy) {
        if (δin == ZERO) {
            assert p == ZERO;
            return δin;
        }
        IntVar δout = intVar(0, getMaxValue());
        IntVar[] vars = new IntVar[]{δin, δout, p};
        table(vars, S_BOXES, strategy).post();
        sboxes.add(new Triple<>(δin, δout, p));
        return δout;
    }

    public List<Triple<IntVar, IntVar, IntVar>> sboxes = new ArrayList<>();

    public Matrix<IntVar> subCell(Matrix<IntVar> δin, Matrix<IntVar> p) {
        return new Matrix<>(δin.getDim1(), δin.getDim2(), (j, k) -> subCell(δin.get(j, k), p.get(j,k )));
    }

    public Matrix<IntVar> shiftRows(Function<Cipher, BlockDimensions> blockSelector, Function<Cipher, int[]> permutations, Matrix<IntVar> input) {
        int[] p = permutations.apply(delegateCipher);
        BlockDimensions dims = blockSelector.apply(delegateCipher);
        return new Matrix<>(input.getDim1(), input.getDim2(), (j, k) -> {
            BlockPosition pos = new BlockPosition(dims, p[new BlockPosition(dims, j, k).ij]);
            return input.get(pos.i, pos.j);
        });
    }

    public void postXor(IntVar a, IntVar b, IntVar c) {
        postXor(a, b, c, DEFAULT_STRATEGY);
    }

    public void postXor(IntVar δfirst, IntVar δsecond, IntVar δthird, String strategy) {
        if (δfirst == ZERO && δsecond == ZERO && δthird == ZERO) {
            return;
        }

        if (δfirst == ZERO && δsecond == ZERO) {
            arithm(δthird, "=", 0).post();
            return;
        }

        if (δsecond == ZERO && δthird == ZERO) {
            arithm(δfirst, "=", 0).post();
            return;
        }

        if (δthird == ZERO && δfirst == ZERO) {
            arithm(δsecond, "=", 0).post();
            return;
        }

        if (δfirst == ZERO) {
            arithm(δsecond, "=", δthird).post();
            return;
        }

        if (δsecond == ZERO) {
            arithm(δfirst, "=", δthird).post();
            return;
        }

        if (δthird == ZERO) {
            arithm(δfirst, "=", δsecond).post();
            return;
        }

        IntVar[] vars = new IntVar[]{δfirst, δsecond, δthird};
        table(vars, XOR, strategy).post();
    }

    public IntVar xor(IntVar δlhs, IntVar δrhs) {
        return xor(δlhs, δrhs, DEFAULT_STRATEGY);
    }

    public IntVar xor(IntVar δlhs, IntVar δrhs, String strategy) {
        if (δlhs == ZERO) return δrhs;
        if (δrhs == ZERO) return δlhs;
        IntVar δout = byteVar();
        postXor(δout, δlhs, δrhs, strategy);
        return δout;
    }

    public IntVar[][] xor(IntVar[][] δlhs, IntVar[][] δrhs) {
        return xor(δlhs, δrhs, DEFAULT_STRATEGY);
    }

    public IntVar[][] xor(IntVar[][] δlhs, IntVar[][] δrhs, String strategy) {
        int n = Math.max(δlhs.length, δrhs.length);
        IntVar[][] result = new IntVar[n][];
        for (int i = 0; i < n; i++) {
            int m = Math.max(δlhs[i].length, δrhs[i].length);
            result[i] = new IntVar[m];
            for (int j = 0; j < m; j++) {
                result[i][j] = xor(δlhs[i][j], δrhs[i][j], strategy);
            }
        }
        return result;
    }

    public IntVar galoisMul(IntVar δin, int n) {
        return galoisMul(δin, n, DEFAULT_STRATEGY_BIN);
    }

    public IntVar galoisMul(IntVar δin, int n, String strategy) {
        if (δin == ZERO) return δin;
        IntVar δout = byteVar();
        IntVar[] vars = new IntVar[]{δin, δout};
        table(vars, GALOIS_FIELD_MULT.computeIfAbsent(n, this::createRelationGaloisFieldMult), strategy).post();
        return δout;
    }

    private Tuples createRelationXor() {
        Tuples tuples = new Tuples(true);
        for (int i = 0; i <= getMaxValue(); i++) {
            for (int j = 0; j <= getMaxValue(); j++) {
                tuples.add(i, j, i ^ j);
            }
        }
        return tuples;
    }

    private Tuples createRelationGaloisFieldMult(int n) {
        Tuples tuples = new Tuples(true);
        for (int i = 0; i <= getMaxValue(); i++) {
            tuples.add(i, galoisFieldMul(n, i));
        }
        return tuples;
    }

    public static int galoisFieldMul(int a, int b) {
        int p = 0;
        for (int counter = 0; counter < 8; counter++) {
            if ((b & 1) != 0) {
                p ^= a;
            }
            boolean highBitSet = (a & 0x80) != 0;
            a <<= 1;
            if (highBitSet) a ^= 0x11B; // 0x1B + remove carry
            b >>= 1;
        }
        return p;
    }

}
