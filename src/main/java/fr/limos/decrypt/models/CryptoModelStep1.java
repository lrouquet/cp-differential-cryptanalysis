package fr.limos.decrypt.models;

import fr.limos.decrypt.ciphering.BlockDimensions;
import fr.limos.decrypt.ciphering.IsBlockCipher;
import fr.limos.decrypt.ciphering.IsStep1Model;
import fr.limos.decrypt.errors.MissingRecordException;
import fr.limos.decrypt.utils.BlockPosition;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Variable;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

public class CryptoModelStep1<Cipher extends IsBlockCipher>
        extends CryptoModel<Cipher>
        implements IsStep1Model {

    private final int sb;

    private final List<IntVar[]> absXor = new ArrayList<>();
    private final Map<String, BoolVar> linearRecords = new HashMap<>();
    private final Map<Long, BoolVar> xorRecords = new HashMap<>();

    public CryptoModelStep1(String name, Cipher cipher, int r, int sb) {
        super(name, cipher, r);
        this.sb = sb;
    }

    @Override
    public int getNbSboxes() {
        return sb;
    }

    public BoolVar[][] emptyAbstractBlock() {
        return emptyAbstractBlock(textBlock().nbRows, textBlock().nbCols);
    }

    public BoolVar[][] emptyAbstractBlock(int nbRows, int nbCols) {
        return new BoolVar[nbRows][nbCols];
    }

    public BoolVar[][] abstractBlock() {
        return abstractBlock(textBlock().nbRows, textBlock().nbCols);
    }

    public BoolVar[][] abstractBlock(int nbRows, int nbCols) {
        BoolVar[][] block = new BoolVar[nbRows][nbCols];
        for (int i = 0; i < nbRows; i++) {
            for (int j = 0; j < nbCols; j++) {
                block[i][j] = delegateModel.boolVar();
            }
        }
        return block;
    }

    public BoolVar subCell(BoolVar input) {
        BoolVar output = delegateModel.boolVar("S(" + input.getName() + ")");
        output.eq(input).post();
        return output;
    }

    public BoolVar[] subCell(BoolVar[] input) {
        BoolVar[] res = new BoolVar[input.length];
        for (int i = 0; i < input.length; i++) {
            res[i] = subCell(input[i]);
        }
        return res;
    }

    public BoolVar[][] subCell(BoolVar[][] input) {
        BoolVar[][] res = new BoolVar[input.length][];
        for (int i = 0; i < input.length; i++) {
            res[i] = subCell(input[i]);
        }
        return res;
    }

    private BoolVar getExistingLinearRecord(String fnName, BoolVar input) {
        String id = identity(fnName, input);
        BoolVar output = linearRecords.get(id);
        if (output == null) throw new MissingRecordException();

        return output;
    }

    private BoolVar getOrCreateLinearRecord(String fnName, BoolVar input) {
        String id = identity(fnName, input);
        BoolVar output = linearRecords.get(id);
        if (output == null) {
            output = delegateModel.boolVar();
            linearRecords.put(id, output);
            BoolVar diff = xor(input, output);
            diff.eq(input).post();
            output.eq(input).post();
        }
        return output;
    }

    public BoolVar linear(String fnName, BoolVar input) {
        return getOrCreateLinearRecord(fnName, input);
    }

    public BoolVar linear(String fnName, BoolVar input, boolean mustBeRecorded) {
        if (mustBeRecorded) {
            return getExistingLinearRecord(fnName, input);
        } else {
            return getOrCreateLinearRecord(fnName, input);
        }
    }

    public void rawXor(IntVar[] values) {
        absXor.add(values);
    }

    public BoolVar xor(BoolVar v) {
        return v;
    }

    public BoolVar xor(BoolVar lhs, BoolVar rhs) {
        if (lhs == rhs) {
            return FALSE; // A xor A = 0
        }
        if (lhs == FALSE) {
            return rhs; // 0 xor B = B
        }
        if (rhs == FALSE) {
            return lhs; // A xor 0 = A
        }

        BoolVar diff = xorRecords.get(identity(lhs, rhs));
        if (diff == null) {
            diff = delegateModel.boolVar(lhs.getName() + " ^ " + rhs.getName());
            recordXor(diff, lhs, rhs);
        }

        return diff;
    }

    public BoolVar xor(BoolVar a, BoolVar b, BoolVar c) {
        BoolVar ab = xor(a, b);
        BoolVar bc = xor(b, c);
        BoolVar ac = xor(a, c);

        BoolVar res = delegateModel.boolVar();
        recordXor(res, a, bc);
        recordXor(res, b, ac);
        recordXor(res, c, ab);

        return res;
    }

    public BoolVar[] xor(BoolVar[] lhs, BoolVar[] rhs) {
        int nbRows = Math.max(lhs.length, rhs.length);
        BoolVar[] res = new BoolVar[nbRows];
        for (int i = 0; i < nbRows; i++) {
            res[i] = xor(lhs[i], rhs[i]);
        }
        return res;
    }

    public BoolVar[][] xor(BoolVar[][] lhs, BoolVar[][] rhs) {
        int nbRows = Math.max(lhs.length, rhs.length);
        BoolVar[][] res = new BoolVar[nbRows][];
        for (int i = 0; i < nbRows; i++) {
            int nbCols = Math.max(lhs[i].length, rhs[i].length);
            res[i] = new BoolVar[nbCols];
            for (int j = 0; j < nbCols; j++) {
                res[i][j] = xor(lhs[i][j], rhs[i][j]);
            }
        }
        return res;
    }

    public BoolVar[][] xor(BoolVar[][] a, BoolVar[][] b, BoolVar[][] c) {
        int nbRows = Math.max(Math.max(a.length, b.length), c.length);
        BoolVar[][] res = new BoolVar[nbRows][];
        for (int i = 0; i < nbRows; i++) {
            int nbCols = Math.max(Math.max(a[i].length, b[i].length), c[i].length);
            res[i] = new BoolVar[nbCols];
            for (int j = 0; j < nbCols; j++) {
                res[i][j] = xor(a[i][j], b[i][j], c[i][j]);
            }
        }
        return res;
    }

    private void recordXor(BoolVar a, BoolVar b, BoolVar c) {
        absXor.add(new IntVar[]{a, b, c});

        xorRecords.put(identity(b, c), a);
        xorRecords.put(identity(a, c), b);
        xorRecords.put(identity(a, b), c);
    }

    private <T> Stream<Map.Entry<T, Integer>> countBy(T[] values) {
        Map<T, Integer> res = new HashMap<>();
        for (T element : values) {
            int c = res.getOrDefault(element, 0);
            res.put(element, c + 1);
        }
        return res.entrySet().stream();
    }

    private String identity(String fnName, Variable v) {
        return fnName + "__VAR__" + v.getId();
    }

    private Long identity(Variable lhs, Variable rhs) {
        if (lhs.getId() < rhs.getId()) {
            return (((long)lhs.getId()) << 32) + rhs.getId();
        } else {
            return (((long)rhs.getId()) << 32) + lhs.getId();
        }
    }

    public BoolVar[][] shiftRows(Function<Cipher, BlockDimensions> blockSelector, Function<Cipher, int[]> permutations, BoolVar[][] input) {
        int[] p = permutations.apply(delegateCipher);
        BlockDimensions dims = blockSelector.apply(delegateCipher);
        BoolVar[][] res = new BoolVar[input.length][];
        for (int i = 0; i < input.length; i++) {
            res[i] = new BoolVar[input[i].length];
            for (int j = 0; j < input[i].length; j++) {
                BlockPosition pos = new BlockPosition(dims, p[new BlockPosition(dims, i, j).ij]);
                res[i][j] = input[pos.i][pos.j];
            }
        }
        return res;
    }

    public void linkWithLinearity(String fnName, BoolVar input, BoolVar output) {
        linearRecords.put(identity(fnName, input), output);
    }
}
