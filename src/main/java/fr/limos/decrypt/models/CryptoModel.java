package fr.limos.decrypt.models;

import fr.limos.decrypt.ciphering.BlockDimensions;
import fr.limos.decrypt.ciphering.IsBlockCipher;
import fr.limos.decrypt.ciphering.IsIterativeModel;
import org.chocosolver.solver.IModel;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;

public class CryptoModel<Cipher extends IsBlockCipher>
        implements IsBlockCipher, IsIterativeModel, IModel {

    public final String DEFAULT_STRATEGY = "FC";
    public final String DEFAULT_STRATEGY_BIN = "FC";

    public final Cipher delegateCipher;
    public final Model delegateModel;
    protected final int r;
    protected final BoolVar FALSE;

    protected CryptoModel(Model model, Cipher delegateCipher, int r) {
        this.delegateCipher = delegateCipher;
        this.delegateModel = model;
        this.r = r;
        FALSE = delegateModel.boolVar(false);
    }

    public CryptoModel(String name, Cipher delegateCipher, int r) {
        this.delegateCipher = delegateCipher;
        this.delegateModel = new Model(name);
        this.r = r;
        FALSE = delegateModel.boolVar(false);
    }

    protected CryptoModel<Cipher> withModel(Model m) {
        return new CryptoModel<>(m, delegateCipher, r);
    }

    public Solver getSolver() {
        return delegateModel.getSolver();
    }

    public int getNbVars() {
        return delegateModel.getNbVars();
    }

    @Override
    public BlockDimensions textBlock() {
        return delegateCipher.textBlock();
    }

    @Override
    public BlockDimensions keyBlock() {
        return delegateCipher.keyBlock();
    }

    @Override
    public int getSeqSize() {
        return delegateCipher.getSeqSize();
    }

    @Override
    public int getNbRounds() {
        return r;
    }

    @Override
    public Model ref() {
        return delegateModel;
    }

    void setObjective(boolean maximize, IntVar objective) {
        delegateModel.setObjective(maximize, objective);
    }

    IntVar[] retrieveIntVars(boolean includeBoolVar) {
        return delegateModel.retrieveIntVars(includeBoolVar);
    }

    public void post(Constraint... cs) {
        delegateModel.post(cs);
    }

}
