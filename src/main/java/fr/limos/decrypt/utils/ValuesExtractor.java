package fr.limos.decrypt.utils;

import com.github.rloic.phd.core.arrays.IntMatrix;
import com.github.rloic.phd.core.arrays.IntTensor3;
import com.github.rloic.phd.core.arrays.Matrix;
import com.github.rloic.phd.core.arrays.Tensor3;
import org.chocosolver.solver.variables.IntVar;

public class ValuesExtractor {

    interface IntArrayInitializer {
        int get(int idx);
    }

    interface IntMatrixInitializer {
        int[] get(int idx);
    }

    interface IntTensor3Initializer {
        int[][] get(int idx);
    }

    private static int[] Array(int len, IntArrayInitializer init) {
        int[] result = new int[len];
        for (int i = 0; i < len; i++) {
            result[i] = init.get(i);
        }
        return result;
    }

    private static int[][] Array(int len, IntMatrixInitializer init) {
        int[][] result = new int[len][];
        for (int i = 0; i < len; i++) {
            result[i] = init.get(i);
        }
        return result;
    }

    private static int[][][] Array(int len, IntTensor3Initializer init) {
        int[][][] result = new int[len][][];
        for (int i = 0; i < len; i++) {
            result[i] = init.get(i);
        }
        return result;
    }

    public static int value(IntVar var) {
        return var.getValue();
    }

    public static int[] values(IntVar[] vars) {
        return Array(vars.length, (IntArrayInitializer) idx -> value(vars[idx]));
    }

    public static IntMatrix values(Matrix<IntVar> vars) {
        return new IntMatrix(vars.getDim1(), vars.getDim2(), (i, j) -> vars.get(i, j).getValue());
    }

    public static Matrix<Integer> valuesOfNullable(Matrix<IntVar> vars) {
        return new Matrix<>(vars.getDim1(), vars.getDim2(), (i, j) -> {
            IntVar variable = vars.get(i, j);
            if (variable == null) {
                return null;
            }
            return variable.getValue();
        });
    }

    public static IntTensor3 values(Tensor3<IntVar> vars) {
        return new IntTensor3(vars.getDim1(), vars.getDim2(), vars.getDim3(), (i, j, k) -> vars.get(i, j, k).getValue());
    }

}
