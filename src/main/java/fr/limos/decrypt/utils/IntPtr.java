package fr.limos.decrypt.utils;

public class IntPtr {

    public int value;

    public IntPtr(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
}
