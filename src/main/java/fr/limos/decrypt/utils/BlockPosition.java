package fr.limos.decrypt.utils;

import fr.limos.decrypt.ciphering.BlockDimensions;

public class BlockPosition {

    public final int i;
    public final int j;
    public final int ij;

    public BlockPosition(BlockDimensions c, int i, int j) {
        this.i = i;
        this.j = j;
        this.ij = i * c.nbCols + j;
    }

    public BlockPosition(BlockDimensions c, int ij) {
        this.ij = ij;
        this.i = ij / c.nbCols;
        this.j = ij - i * c.nbCols;
    }

}
