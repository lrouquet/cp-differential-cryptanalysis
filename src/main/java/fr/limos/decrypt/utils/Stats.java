package fr.limos.decrypt.utils;

public class Stats {

    private int nbSols;
    private long decisionCount;
    private long failCount;
    private long backtrackCount;

    public Stats() {
        this(0, 0L, 0L, 0L);
    }

    public Stats(int nbSols, long decisionCount, long failCount, long backtrackCount) {
        this.nbSols = nbSols;
        this.decisionCount = decisionCount;
        this.failCount = failCount;
        this.backtrackCount = backtrackCount;
    }

    public Stats plus(Stats other) {
        return new Stats(
                nbSols + other.nbSols,
                decisionCount + other.decisionCount,
                failCount + other.failCount,
                backtrackCount + other.backtrackCount
        );
    }

    public Stats minus(Stats other) {
        return new Stats(
                nbSols - other.nbSols,
                decisionCount - other.decisionCount,
                failCount - other.failCount,
                backtrackCount - other.backtrackCount
        );
    }

    public void plusAssign(Stats other) {
        nbSols += other.nbSols;
        decisionCount += other.decisionCount;
        failCount += other.failCount;
        backtrackCount += other.backtrackCount;
    }

    public int getNbSols() {
        return nbSols;
    }

    public long getFailCount() {
        return failCount;
    }

    public long getDecisionCount() {
        return decisionCount;
    }

    public long getBacktrackCount() {
        return backtrackCount;
    }

}
