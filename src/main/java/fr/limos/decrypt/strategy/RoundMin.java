package fr.limos.decrypt.strategy;

import gnu.trove.list.array.TIntArrayList;
import org.chocosolver.memory.IStateInt;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.search.loop.monitors.IMonitorContradiction;
import org.chocosolver.solver.search.strategy.assignments.DecisionOperatorFactory;
import org.chocosolver.solver.search.strategy.decision.Decision;
import org.chocosolver.solver.search.strategy.selectors.values.IntValueSelector;
import org.chocosolver.solver.search.strategy.strategy.AbstractStrategy;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.objects.IntMap;

import java.util.Comparator;
import java.util.stream.Stream;

public class RoundMin extends AbstractStrategy<IntVar> implements IMonitorContradiction {

   private final IntValueSelector valueSelector;
   private final Comparator<IntVar> roundOf;

   private final IntMap pid2arity;
   private final TIntArrayList bests;
   private final java.util.Random random;
   private final IStateInt last;
   protected final IntMap p2w;

   public RoundMin(IntVar[] variables, Comparator<IntVar> roundOf, long seed, IntValueSelector selector)  {
      super(variables);
      this.valueSelector = selector;
      this.roundOf = roundOf;

      Model model = variables[0].getModel();
      pid2arity = new IntMap(model.getCstrs().length * 3 / 2 + 1, -1);
      bests = new TIntArrayList();
      random = new java.util.Random(seed);
      this.last = model.getEnvironment().makeInt(vars.length - 1);
      p2w = new IntMap(10, 0);
      init(Stream.of(model.getCstrs())
            .flatMap(c -> Stream.of(c.getPropagators()))
            .toArray(Propagator[]::new));
   }

   private void init(Propagator[] propagators) {
      for (Propagator propagator : propagators) {
         p2w.put(propagator.getId(), 0);
      }
   }
   @Override protected Decision<IntVar> computeDecision(IntVar var) {
      if (var == null || var.isInstantiated()) return null;
      return var.getModel().getSolver().getDecisionPath().makeIntDecision(var, DecisionOperatorFactory.makeIntEq(), valueSelector.selectValue(var));
   }

   @Override public Decision<IntVar> getDecision() {
      IntVar best = null;
      bests.resetQuick();
      pid2arity.clear();
      IntVar bestVar = null;
      long _d1 = Integer.MAX_VALUE;
      long _d2 = 0;
      int to = last.get();
      for (int idx = 0; idx <= to; idx++) {
         int dsize = vars[idx].getDomainSize();
         if (dsize > 1) {
            IntVar round = vars[idx];
            if (bestVar == null || roundOf.compare(round, bestVar) > 0) {
               bests.resetQuick();
               bests.add(idx);
               bestVar = round;
            } else if (roundOf.compare(round, bestVar) == 0) {
               int weight = weight(vars[idx]);
               long c1 = dsize * _d2;
               long c2 = _d1 * weight;
               if (c1 < c2) {
                  bests.resetQuick();
                  bests.add(idx);
                  _d1 = dsize;
                  _d2 = weight;
               } else if (c1 == c2) {
                  bests.add(idx);
               }
            }
         } else {
            // swap
            IntVar tmp = vars[to];
            vars[to] = vars[idx];
            vars[idx] = tmp;
            idx--;
            to--;
         }
      }
      last.set(to);
      if (bests.size() > 0) {
         int currentVar = bests.get(random.nextInt(bests.size()));
         best = vars[currentVar];
      }
      return computeDecision(best);
   }

   @Override
   public boolean init() {
      Solver solver = vars[0].getModel().getSolver();
      if(!solver.getSearchMonitors().contains(this)) {
         vars[0].getModel().getSolver().plugMonitor(this);
      }
      return true;
   }

   @Override
   public void remove() {
      Solver solver = vars[0].getModel().getSolver();
      if(solver.getSearchMonitors().contains(this)) {
         vars[0].getModel().getSolver().unplugMonitor(this);
      }
   }

   @Override
   public void onContradiction(ContradictionException cex) {
      if (cex.c instanceof Propagator) {
         Propagator p = (Propagator) cex.c;
         p2w.putOrAdjust(p.getId(), 1, 1);
      }
   }

   private int weight(IntVar v) {
      int w = 1;
      int nbp = v.getNbProps();
      for (int i = 0; i < nbp; i++) {
         Propagator prop = v.getPropagator(i);
         int pid = prop.getId();
         // if the propagator has been already evaluated
         if (pid2arity.get(pid) > -1) {
            w += p2w.get(prop.getId());
         } else {
            // the arity of this propagator is not yet known
            int futVars = prop.arity();
            assert futVars > -1;
            pid2arity.put(pid, futVars);
            if (futVars > 1) {
               w += p2w.get(prop.getId());
            }
         }
      }
      return w;
   }

}
