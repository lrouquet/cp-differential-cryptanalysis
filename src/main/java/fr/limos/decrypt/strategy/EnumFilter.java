package fr.limos.decrypt.strategy;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.nary.sat.PropSat;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.variables.BoolVar;

public class EnumFilter implements IMonitorSolution {

    private final Model m;
    private final BoolVar[] sBoxes;
    private final int objStep1;
    private final PropSat psat;

    public EnumFilter(Model model, BoolVar[] sBoxes, int objStep1) {
        this.m = model;
        this.sBoxes = sBoxes;
        this.objStep1 = objStep1;
        this.psat = m.getMinisat().getPropSat();
    }

    @Override
    public void onSolution() {
        int[] lits = new int[objStep1];
        int cpt = 0;
        for (BoolVar s : sBoxes) {
            if (!s.isInstantiated()) {
                throw new RuntimeException("SBoxes not instantiated on solution");
            }
            if (s.getValue() == 1) {
                lits[cpt++] = psat.makeLiteral(s, false);
            }
        }
        psat.addLearnt(lits);
    }

}
