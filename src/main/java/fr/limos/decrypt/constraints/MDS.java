package fr.limos.decrypt.constraints;

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;

public class MDS extends Propagator<IntVar> {

    private final IntVar lhs;
    private final IntVar rhs;

    public MDS(IntVar lhs, IntVar rhs) {
        super(lhs, rhs);
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        if (lhs.isInstantiatedTo(0)) {
            rhs.instantiateTo(0, this);
        }

        if (rhs.isInstantiatedTo(0)) {
            lhs.instantiateTo(0, this);
        }

        if (lhs.getLB() > 0) {
            rhs.updateLowerBound(5 - lhs.getUB(), this);
        }

        if (rhs.getLB() > 0) {
            lhs.updateLowerBound(5 - rhs.getUB(), this);
        }
    }

    @Override
    public ESat isEntailed() {
        if (lhs.isInstantiatedTo(0) && rhs.getLB() > 0) return ESat.FALSE;
        if (rhs.isInstantiatedTo(0) && lhs.getLB() > 0) return ESat.FALSE;

        if (lhs.isInstantiatedTo(0) && rhs.isInstantiatedTo(0)) {
            return ESat.TRUE;
        } else if (lhs.getLB() + rhs.getLB() > 5) {
            return ESat.TRUE;
        }

        return ESat.UNDEFINED;
    }
}
