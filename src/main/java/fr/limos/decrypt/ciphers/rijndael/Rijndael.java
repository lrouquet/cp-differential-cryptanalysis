package fr.limos.decrypt.ciphers.rijndael;

import com.github.rloic.phd.core.arrays.Matrix;
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Configuration;
import fr.limos.decrypt.ciphering.BlockDimensions;
import fr.limos.decrypt.ciphering.IsBlockCipher;
import fr.limos.decrypt.ciphering.HasSBoxes;
import fr.limos.decrypt.models.CryptoModelStep1;
import fr.limos.decrypt.models.CryptoModelStep2;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;

import static com.github.rloic.phd.core.arrays.MatrixKt.matrixOfNulls;

public class Rijndael implements IsBlockCipher, HasSBoxes {

    public static int[] MDS = {0, 5, 6, 7, 8};
    private final static int[] S_BOXES = {
            99, 124, 119, 123, 242, 107, 111, 197, 48, 1, 103, 43, 254, 215, 171, 118,
            202, 130, 201, 125, 250, 89, 71, 240, 173, 212, 162, 175, 156, 164, 114, 192,
            183, 253, 147, 38, 54, 63, 247, 204, 52, 165, 229, 241, 113, 216, 49, 21,
            4, 199, 35, 195, 24, 150, 5, 154, 7, 18, 128, 226, 235, 39, 178, 117,
            9, 131, 44, 26, 27, 110, 90, 160, 82, 59, 214, 179, 41, 227, 47, 132,
            83, 209, 0, 237, 32, 252, 177, 91, 106, 203, 190, 57, 74, 76, 88, 207,
            208, 239, 170, 251, 67, 77, 51, 133, 69, 249, 2, 127, 80, 60, 159, 168,
            81, 163, 64, 143, 146, 157, 56, 245, 188, 182, 218, 33, 16, 255, 243, 210,
            205, 12, 19, 236, 95, 151, 68, 23, 196, 167, 126, 61, 100, 93, 25, 115,
            96, 129, 79, 220, 34, 42, 144, 136, 70, 238, 184, 20, 222, 94, 11, 219,
            224, 50, 58, 10, 73, 6, 36, 92, 194, 211, 172, 98, 145, 149, 228, 121,
            231, 200, 55, 109, 141, 213, 78, 169, 108, 86, 244, 234, 101, 122, 174, 8,
            186, 120, 37, 46, 28, 166, 180, 198, 232, 221, 116, 31, 75, 189, 139, 138,
            112, 62, 181, 102, 72, 3, 246, 14, 97, 53, 87, 185, 134, 193, 29, 158,
            225, 248, 152, 17, 105, 217, 142, 148, 155, 30, 135, 233, 206, 85, 40, 223,
            140, 161, 137, 13, 191, 230, 66, 104, 65, 153, 45, 15, 176, 84, 187, 22
    };

    static class ParseException extends RuntimeException {
        public ParseException(String msg) {
            super(msg);
        }
    }

    private final Size textSize;
    private final Size keySize;

    public enum Size {
        RIJNDAEL_128(new BlockDimensions(128, 4, 4), new int[]{0, 1, 2, 3}),
        RIJNDAEL_160(new BlockDimensions(160, 4, 5), new int[]{0, 1, 2, 3}),
        RIJNDAEL_192(new BlockDimensions(192, 4, 6), new int[]{0, 1, 2, 3}),
        RIJNDAEL_224(new BlockDimensions(224, 4, 7), new int[]{0, 1, 2, 4}),
        RIJNDAEL_256(new BlockDimensions(256, 4, 8), new int[]{0, 1, 3, 4});

        final BlockDimensions dims;
        final int[] shiftOffsets;

        Size(BlockDimensions dims, int[] shiftOffsets) {
            this.dims = dims;
            this.shiftOffsets = shiftOffsets;
        }

        public static Size fromInt(int size) {
            switch (size) {
                case 128:
                    return RIJNDAEL_128;
                case 160:
                    return RIJNDAEL_160;
                case 192:
                    return RIJNDAEL_192;
                case 224:
                    return RIJNDAEL_224;
                case 256:
                    return RIJNDAEL_256;
                default:
                    throw new ParseException("Cannot parse \"" + size + "\" as Rijndael size, valid values are {128, 160, 192, 224, 256}");
            }
        }

        public static Size fromString(String str) {
            switch (str) {
                case "128":
                    return RIJNDAEL_128;
                case "160":
                    return RIJNDAEL_160;
                case "192":
                    return RIJNDAEL_192;
                case "224":
                    return RIJNDAEL_224;
                case "256":
                    return RIJNDAEL_256;
                default:
                    throw new ParseException("Cannot parse \"" + str + "\" as Rijndael size, valid values are {128, 160, 192, 224, 256}");
            }
        }

    }

    public Rijndael(Configuration configuration) {
        this.textSize = Size.fromInt(configuration.getTextSize());
        this.keySize = Size.fromInt(configuration.getKeySize());
    }

    public Rijndael(Size textSize, Size keySize) {
        this.textSize = textSize;
        this.keySize = keySize;
    }

    @Override
    public BlockDimensions textBlock() {
        return textSize.dims;
    }

    @Override
    public BlockDimensions keyBlock() {
        return keySize.dims;
    }

    @Override
    public int getSeqSize() {
        return 8;
    }

    public int[] shiftOffsets() {
        int[] shiftOffsets = new int[textSize.dims.nbRows * textSize.dims.nbCols];

        for (int i = 0; i < textSize.dims.nbRows; i++) {
            for (int j = 0; j < textSize.dims.nbCols; j++) {
                shiftOffsets[i * textSize.dims.nbCols + j] = i * textSize.dims.nbCols + ((j + textSize.shiftOffsets[i]) % textSize.dims.nbCols);
            }
        }

        return shiftOffsets;
    }

    @Deprecated
    public boolean isSbColumn(int k) {
        int Nk = keySize.dims.nbCols;
        return (k >= Nk - 1) && ((k % Nk == Nk - 1) || (Nk > 6 && k % Nk == 3));
    }

    @Override
    public Tuples createRelationSBoxes() {
        int[][] trans = new int[256][127];
        int[][] probas = new int[256][256];
        int[] ctrans = new int[256];
        Tuples tuples = new Tuples(true);

        for (int i = 0; i < 256; i++) {
            ctrans[i] = 0;
            for (int j = 0; j < 256; j++) {
                probas[i][j] = 0;
                if (j < 127) {
                    trans[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < 256; i++) {
            for (int j = 0; j < 256; j++) {
                probas[i][(S_BOXES[j] ^ S_BOXES[j ^ i])]++;
                if (probas[i][(S_BOXES[j] ^ S_BOXES[j ^ i])] == 1) {
                    trans[i][ctrans[i]++] = S_BOXES[j] ^ S_BOXES[i ^ j];
                }
            }
        }
        tuples.add(0, 0, 0);
        for (int i = 1; i < 256; i++) {
            for (int j = 0; j < 127; j++) {
                int p = probas[i][trans[i][j]] / 2;
                tuples.add(i, trans[i][j], p - 8);
            }
        }
        return tuples;
    }

    @Override
    public Tuples createRelationSBoxesNoP() {
        int[][] trans = new int[256][127];
        int[][] probas = new int[256][256];
        int[] ctrans = new int[256];
        Tuples tuples = new Tuples(true);

        for (int i = 0; i < 256; i++) {
            ctrans[i] = 0;
            for (int j = 0; j < 256; j++) {
                probas[i][j] = 0;
                if (j < 127) {
                    trans[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < 256; i++) {
            for (int j = 0; j < 256; j++) {
                probas[i][(S_BOXES[j] ^ S_BOXES[j ^ i])]++;
                if (probas[i][(S_BOXES[j] ^ S_BOXES[j ^ i])] == 1) {
                    trans[i][ctrans[i]++] = S_BOXES[j] ^ S_BOXES[i ^ j];
                }
            }
        }
        tuples.add(0, 0);
        for (int i = 1; i < 256; i++) {
            for (int j = 0; j < 127; j++) {
                tuples.add(i, trans[i][j]);
            }
        }
        return tuples;
    }

    interface BlockInitializer<T> {
        T create(int i, int j);
    }

    protected static IntVar[][] block(int rows, int cols, BlockInitializer<IntVar> init) {
        IntVar[][] res = new IntVar[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                res[i][j] = init.create(i, j);
            }
        }
        return res;
    }

    @SuppressWarnings("NonAsciiCharacters")
    public static Matrix<BoolVar> mixColumn(CryptoModelStep1<Rijndael> m, Matrix<BoolVar> ΔY) {
        Matrix<BoolVar> ΔZ = new Matrix<>(m.textRows(), m.textCols(), (i, k) ->
                m.boolVar()
        );

        IntVar[] colY = m.intVarArray(m.textCols(), 0 ,4);
        IntVar[] colZ = m.intVarArray(m.textCols(), 0 ,4);

        for (int k = 0; k < m.textCols(); k++) {
            m.sum(new IntVar[]{ ΔY.get(0, k), ΔY.get(1, k), ΔY.get(2, k), ΔY.get(3, k) }, "=", colY[k]).post();
            m.sum(new IntVar[]{ ΔZ.get(0, k), ΔZ.get(1, k), ΔZ.get(2, k), ΔZ.get(3, k) }, "=", colZ[k]).post();
            m.ifOnlyIf(
                    m.arithm(colY[k], "=", 0),
                    m.arithm(colZ[k], "=", 0)
            );
            m.arithm(colY[k], "+", colZ[k], "=", m.intVar(MDS)).post();
        }

        return ΔZ;
    }

    @SuppressWarnings("NonAsciiCharacters")
    public static Matrix<IntVar> mixColumn(CryptoModelStep2<Rijndael> m, Matrix<IntVar> δY) {
        Matrix<IntVar> δ2Y = new Matrix<>(m.textRows(), m.textCols(), (i, j) -> m.galoisMul(δY.get(i, j), 2));
        Matrix<IntVar> δ3Y = new Matrix<>(m.textRows(), m.textCols(), (j, k) -> m.xor(δY.get(j, k), δ2Y.get(j, k)));

        Matrix<IntVar> δZ = matrixOfNulls(m.textRows(), m.textCols());
        for (int k = 0; k < m.textCols(); k++) {
            δZ.set(0, k, m.xor(m.xor(δ2Y.get(0, k), δ3Y.get(1, k)), m.xor(δY.get(2, k), δY.get(3, k))));
            δZ.set(1, k, m.xor(m.xor(δY.get(0, k), δ2Y.get(1, k)), m.xor(δ3Y.get(2, k), δY.get(3, k))));
            δZ.set(2, k, m.xor(m.xor(δY.get(0, k), δY.get(1, k)), m.xor(δ2Y.get(2, k), δ3Y.get(3, k))));
            δZ.set(3, k, m.xor(m.xor(δ3Y.get(0, k), δY.get(1, k)), m.xor(δY.get(2, k), δ2Y.get(3, k))));
        }

        Matrix<IntVar> δ9Z = new Matrix<>(m.textRows(), m.textCols(), (j, k) -> m.galoisMul(δZ.get(j, k), 9));
        Matrix<IntVar> δ11Z = new Matrix<>(m.textRows(), m.textCols(), (j, k) -> m.galoisMul(δZ.get(j, k), 11));
        Matrix<IntVar> δ13Z = new Matrix<>(m.textRows(), m.textCols(), (j, k) -> m.galoisMul(δZ.get(j, k), 13));
        Matrix<IntVar> δ14Z = new Matrix<>(m.textRows(), m.textCols(), (j, k) -> m.galoisMul(δZ.get(j, k), 14));

        for (int k = 0; k < m.textCols(); k++) {
            m.postXor(δY.get(0, k), m.xor(δ14Z.get(0, k), δ11Z.get(1, k)), m.xor(δ13Z.get(2, k), δ9Z.get(3, k)));
            m.postXor(δY.get(1, k), m.xor(δ9Z.get(0, k), δ14Z.get(1, k)), m.xor(δ11Z.get(2, k), δ13Z.get(3, k)));
            m.postXor(δY.get(2, k), m.xor(δ13Z.get(0, k), δ9Z.get(1, k)), m.xor(δ14Z.get(2, k), δ11Z.get(3, k)));
            m.postXor(δY.get(3, k), m.xor(δ11Z.get(0, k), δ13Z.get(1, k)), m.xor(δ9Z.get(2, k), δ14Z.get(3, k)));
        }

        return δZ;
    }

    @SuppressWarnings("NonAsciiCharacters")
    public static IntVar[][] mixColumnForward(CryptoModelStep2<Rijndael> m, IntVar[][] δZ) {
        IntVar[][] δY = m.intVarMatrix(m.textRows(), m.textCols(), 0, m.getMaxValue());

        IntVar[][] δ9Z = block(m.textRows(), m.textCols(), (i, j) -> m.galoisMul(δZ[i][j], 9));
        IntVar[][] δ11Z = block(m.textRows(), m.textCols(), (i, j) -> m.galoisMul(δZ[i][j], 11));
        IntVar[][] δ13Z = block(m.textRows(), m.textCols(), (i, j) -> m.galoisMul(δZ[i][j], 13));
        IntVar[][] δ14Z = block(m.textRows(), m.textCols(), (i, j) -> m.galoisMul(δZ[i][j], 14));

        for (int k = 0; k < m.textCols(); k++) {
            δY[0][k] = m.xor(m.xor(δ14Z[0][k], δ11Z[1][k]), m.xor(δ13Z[2][k], δ9Z[3][k]));
            δY[1][k] = m.xor(m.xor(δ9Z[0][k], δ14Z[1][k]), m.xor(δ11Z[2][k], δ13Z[3][k]));
            δY[2][k] = m.xor(m.xor(δ13Z[0][k], δ9Z[1][k]), m.xor(δ14Z[2][k], δ11Z[3][k]));
            δY[3][k] = m.xor(m.xor(δ11Z[0][k], δ13Z[1][k]), m.xor(δ9Z[2][k], δ14Z[3][k]));
        }

        return δY;
    }

    @SuppressWarnings("NonAsciiCharacters")
    public static IntVar[][] mixColumnBackward(CryptoModelStep2<Rijndael> m, IntVar[][] δY) {
        IntVar[][] δ2Y = block(m.textRows(), m.textCols(), (i, j) -> m.galoisMul(δY[i][j], 2));
        IntVar[][] δ3Y = block(m.textRows(), m.textCols(), (i, j) -> m.xor(δY[i][j], δ2Y[i][j]));

        IntVar[][] δZ = m.intVarMatrix(m.textRows(), m.textCols(), 0, m.getMaxValue());
        for (int k = 0; k < m.textCols(); k++) {
            δZ[0][k] = m.xor(m.xor(δ2Y[0][k], δ3Y[1][k]), m.xor(δY[2][k], δY[3][k]));
            δZ[1][k] = m.xor(m.xor(δY[0][k], δ2Y[1][k]), m.xor(δ3Y[2][k], δY[3][k]));
            δZ[2][k] = m.xor(m.xor(δY[0][k], δY[1][k]), m.xor(δ2Y[2][k], δ3Y[3][k]));
            δZ[3][k] = m.xor(m.xor(δ3Y[0][k], δY[1][k]), m.xor(δY[2][k], δ2Y[3][k]));
        }

        return δZ;
    }

}
