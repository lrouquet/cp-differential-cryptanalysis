package fr.limos.decrypt.ciphers.rijndael;

import com.github.rloic.phd.core.arrays.Matrix;
import fr.limos.decrypt.models.CryptoModelStep2;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.IntVar;

public class RijndaelGLMS20 extends Rijndael {

    private static final Tuples tupleMul2xorMul3 = createRelationMul2xorMul3();

    public RijndaelGLMS20(Size textSize, Size keySize) {
        super(textSize, keySize);
    }

    @SuppressWarnings("NonAsciiCharacters")
    public static Matrix<IntVar> mixColumn(CryptoModelStep2<Rijndael> m, Matrix<IntVar> δY) {
        Matrix<IntVar> δZ = new Matrix<>(m.textRows(), m.textRows(), (j, k) -> m.intVar(0, 255));
        postMC(m, δY, δZ);
        postMCInv(m, δZ, δY);
        return δZ;
    }

    @SuppressWarnings("NonAsciiCharacters")
    private static void postMC(CryptoModelStep2<Rijndael> m, Matrix<IntVar> δY, Matrix<IntVar> δZ) {
        for (int k = 0; k < m.textCols(); k++) {
            IntVar v1 = m.intVar(0, 255);
            IntVar v2 = m.intVar(0, 255);
            IntVar v3 = m.intVar(0, 255);
            IntVar v4 = m.intVar(0, 255);
            IntVar v5 = m.intVar(0, 255);
            IntVar v6 = m.intVar(0, 255);
            IntVar v7 = m.intVar(0, 255);
            IntVar v8 = m.intVar(0, 255);

            m.table(new IntVar[]{v1, δY.get(0, k), δY.get(1, k)}, tupleMul2xorMul3).post(); // v1 = MUL2*SR[0][i] xor MUL3*SR[1][i]
            m.postXor(v1, δY.get(2, k), v2); // v2 = v1 xor SR[2][i]
            m.postXor(v2, δY.get(3, k), δZ.get(0, k)); // Y[0][i] = v2 xor SR[3][i]

            m.table(new IntVar[]{v3, δY.get(1, k), δY.get(2, k)}, tupleMul2xorMul3).post(); // v3 = MUL2*SR[1][i] xor MUL3*SR[2][i]
            m.postXor(v3, δY.get(0, k), v4); // v4 = v3 xor SR[0][i]
            m.postXor(v4, δY.get(3, k), δZ.get(1, k)); // Y[1][i] = v4 xor SR[3][i]
            m.table(new IntVar[]{v5, δY.get(2, k), δY.get(3, k)}, tupleMul2xorMul3).post(); // v5 = MUL2*SR[2][i] xor MUL3*SR[3][i]
            m.postXor(v5, δY.get(0, k), v6); // v6 = v5 xor SR[2][i]
            m.postXor(v6, δY.get(1, k), δZ.get(2, k)); // Y[2][i] = v6 xor SR[1][i]
            m.table(new IntVar[]{v7, δY.get(3, k), δY.get(0, k)}, tupleMul2xorMul3).post(); // v7 = MUL2*SR[3][i] xor MUL3*SR[0][i]
            m.postXor(v7, δY.get(1, k), v8); // v8 = v7 xor SR[1][i]
            m.postXor(v8, δY.get(2, k), δZ.get(3, k)); // Y[3][i] = v8 xor SR[2][i]
        }
    }

    @SuppressWarnings("NonAsciiCharacters")
    private static void postMCInv(CryptoModelStep2<Rijndael> m, Matrix<IntVar> δZ, Matrix<IntVar> δY) {
        for (int k = 0; k < m.textCols(); k++) {
            IntVar[] tmp = new IntVar[]{δZ.get(0, k), δZ.get(1, k), δZ.get(2, k), δZ.get(3, k)};
            IntVar[] tmp2 = new IntVar[]{δY.get(0, k), δY.get(1, k), δY.get(2, k), δY.get(3, k)};

            m.ifThen(
                    m.sum(tmp, "=", 0),
                    m.sum(tmp2, "=", 0)
            );

            IntVar[] tmpMC = m.intVarArray("tmpMC", 24, 0, 255);

            m.table(new IntVar[]{tmp[0], tmpMC[0]}, mul14).post();
            m.table(new IntVar[]{tmp[1], tmpMC[1]}, mul11).post();
            m.table(new IntVar[]{tmp[2], tmpMC[2]}, mul13).post();
            m.table(new IntVar[]{tmp[3], tmpMC[3]}, mul9).post();

            m.table(new IntVar[]{tmp[0], tmpMC[4]}, mul9).post();
            m.table(new IntVar[]{tmp[1], tmpMC[5]}, mul14).post();
            m.table(new IntVar[]{tmp[2], tmpMC[6]}, mul11).post();
            m.table(new IntVar[]{tmp[3], tmpMC[7]}, mul13).post();

            m.table(new IntVar[]{tmp[0], tmpMC[8]}, mul13).post();
            m.table(new IntVar[]{tmp[1], tmpMC[9]}, mul9).post();
            m.table(new IntVar[]{tmp[2], tmpMC[10]}, mul14).post();
            m.table(new IntVar[]{tmp[3], tmpMC[11]}, mul11).post();

            m.table(new IntVar[]{tmp[0], tmpMC[12]}, mul11).post();
            m.table(new IntVar[]{tmp[1], tmpMC[13]}, mul13).post();
            m.table(new IntVar[]{tmp[2], tmpMC[14]}, mul9).post();
            m.table(new IntVar[]{tmp[3], tmpMC[15]}, mul14).post();

            m.postXor(tmpMC[0], tmpMC[1], tmpMC[16]);
            m.postXor(tmpMC[2], tmpMC[3], tmpMC[17]);
            m.postXor(tmpMC[16], tmpMC[17], tmp2[0]);

            m.postXor(tmpMC[4], tmpMC[5], tmpMC[18]);
            m.postXor(tmpMC[6], tmpMC[7], tmpMC[19]);
            m.postXor(tmpMC[18], tmpMC[19], tmp2[1]);

            m.postXor(tmpMC[8], tmpMC[9], tmpMC[20]);
            m.postXor(tmpMC[10], tmpMC[11], tmpMC[21]);
            m.postXor(tmpMC[20], tmpMC[21], tmp2[2]);

            m.postXor(tmpMC[12], tmpMC[13], tmpMC[22]);
            m.postXor(tmpMC[14], tmpMC[15], tmpMC[23]);
            m.postXor(tmpMC[22], tmpMC[23], tmp2[3]);
        }
    }

    private static Tuples createRelationMul2xorMul3() {
        Tuples tuples = new Tuples(true);
        for (int i = 0; i < 256; i++) {
            int ii;
            if (i < 128) ii = 2 * i;
            else ii = (2 * i % 256) ^ 27;
            for (int j = 0; j < 256; j++) {
                int jj;
                if (j < 128) jj = (2 * j) ^ j;
                else jj = ((2 * j % 256) ^ 27) ^ j;
                tuples.add(ii ^ jj, i, j);

            }
        }
        return tuples;
    }

    // Lookup table for multiplying by 9 in Rijndael's finite field
    private static Tuples mul9 = initMul9();

    private static Tuples initMul9() {
        Tuples ret = new Tuples(true);
        for (int i = 0; i < 256; i++) {
            ret.add(i, CryptoModelStep2.galoisFieldMul(i, 9));
        }

        return ret;
    }

    // Lookup table for multiplying by 11 in Rijndael's finite field
    private static Tuples mul11 = initMul11();

    private static Tuples initMul11() {
        Tuples ret = new Tuples(true);

        for (int i = 0; i < 256; i++) {
            ret.add(i, CryptoModelStep2.galoisFieldMul(i, 11));
        }

        return ret;
    }

    // Lookup table for multiplying by 13 in Rijndael's finite field
    private static Tuples mul13 = initMul13();

    private static Tuples initMul13() {
        Tuples ret = new Tuples(true);

        for (int i = 0; i < 256; i++) {
            ret.add(i, CryptoModelStep2.galoisFieldMul(i, 13));
        }

        return ret;
    }

    // Lookup table for multiplying by 14 in Rijndael's finite field
    private static Tuples mul14 = initMul14();

    private static Tuples initMul14() {
        Tuples ret = new Tuples(true);

        for (int i = 0; i < 256; i++) {
            ret.add(i, CryptoModelStep2.galoisFieldMul(i, 14));
        }

        return ret;
    }


}
