package fr.limos.decrypt.errors;

public class MissingRecordException extends RuntimeException {

    public MissingRecordException() {
        super("The variable should have already been recorded");
    }

}
