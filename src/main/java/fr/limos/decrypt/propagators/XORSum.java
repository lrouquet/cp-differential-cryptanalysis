package fr.limos.decrypt.propagators;

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.events.IntEventType;
import org.chocosolver.util.ESat;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableBitSet;
import org.chocosolver.util.objects.setDataStructures.iterable.IntIterableSet;

public class XORSum extends Propagator<IntVar> {

   private final IntVar A;
   private final IntVar B;
   private final IntVar C;

   public XORSum(IntVar A, IntVar B, IntVar C) {
      super(new IntVar[]{A, B, C}, PropagatorPriority.TERNARY, false);
      this.A = A;
      this.B = B;
      this.C = C;
   }

   private void makeEquals(IntVar a, IntVar b) throws ContradictionException {
      IntVar smallestDomain = a.getDomainSize() < b.getDomainSize() ? a : b;
      IntVar largestDomain = a.getDomainSize() < b.getDomainSize() ? b : a;
      IntIterableSet intersection = new IntIterableBitSet();
      boolean subSetEq = true;
      for (int v : smallestDomain) {
         if (largestDomain.contains(v)) {
            intersection.add(v);
         } else {
            subSetEq = false;
         }
      }
      if (smallestDomain.getDomainSize() != largestDomain.getDomainSize() || !subSetEq) {
         a.removeAllValuesBut(intersection, this);
         b.removeAllValuesBut(intersection, this);
      }
   }

   @Override
   public int getPropagationConditions(int vIdx) {
      return IntEventType.combine(IntEventType.INSTANTIATE, IntEventType.INCLOW, IntEventType.DECUPP);
   }


   @Override
   public void propagate(int evtmask) throws ContradictionException {
      if (eq(A, 0) && eq(B, 0)) {
         C.updateUpperBound(0, this);
      }

      if (eq(B, 0) && eq(C, 0)) {
         A.updateUpperBound(0, this);
      }

      if (eq(A, 0) && eq(C, 0)) {
         B.updateUpperBound(0, this);
      }

      if (eq(A, 0)) {
         makeEquals(B, C);
      }
      if (eq(B, 0)) {
         makeEquals(A, C);
      }
      if (eq(C, 0)) {
         makeEquals(A, B);
      }

      if (gt(A, B)) { // A - B <= C <= A + B
         C.updateLowerBound(A.getLB() - B.getUB(), this);
      } else { // B - A <= C <= A + B
         C.updateLowerBound(B.getLB() - A.getUB(), this);
      }
      C.updateUpperBound(A.getUB() + B.getUB(), this);

      if (gt(B, C)) { // B - C <= A <= B + C
         A.updateLowerBound(B.getLB() - C.getUB(), this);
      } else { // C - B <= A <= B + C
         A.updateLowerBound(C.getLB() - B.getUB(), this);
      }
      A.updateUpperBound(B.getUB() + C.getUB(), this);

      if (gt(A, C)) { // A - C <= B <= A + C
         B.updateLowerBound(A.getLB() - C.getUB(), this);
      } else { // C - A <= B <= A + C
         B.updateLowerBound(C.getLB() - A.getUB(), this);
      }
      B.updateUpperBound(A.getUB() + C.getUB(), this);
   }

   private boolean isDiff(IntVar A, IntVar B) {
      for (int v : A) {
         if (B.contains(v)) return false;
      }
      return true;
   }

   private boolean eq(IntVar v, int k) {
      return v.isInstantiatedTo(k);
   }

   private boolean gt(IntVar v, int k) {
      return v.getLB() > k;
   }

   private boolean gt(IntVar v1, IntVar v2) {
      return v1.getLB() > v2.getUB();
   }

   private boolean gte(IntVar v1, IntVar v2) {
      return v1.getLB() >= v2.getUB();
   }

   private boolean gte(IntVar v, int k) {
      return v.getLB() >= k;
   }

   private boolean lte(IntVar v, int k) {
      return v.getUB() < k;
   }

   @Override
   public ESat isEntailed() {
      if (A.isInstantiated() && B.isInstantiated() && C.isInstantiated()) {
         int a = A.getValue();
         int b = B.getValue();
         int c = C.getValue();

         if (a <= b + c && b <= a + c && c <= a + b) {
            return ESat.TRUE;
         } else {
            return ESat.FALSE;
         }
      }

      if (eq(A, 0) && gt(B, 0) && isDiff(B, C)) { // A = 0 ^ B > 0 => C = B
         return ESat.FALSE;
      } else if (gt(A, 0) && eq(B, 0) && isDiff(A, C)) { // A > 0 ^ B = 0 => C = A
         return ESat.FALSE;
      } else if (gt(A, 0) && gt(B, A)) { // A > 0 ^ B > A => B - A <= C <= A + B
         int bMinusA = B.getLB() - A.getUB();
         int aPlusB = A.getUB() + B.getUB();
         if (!gte(C, bMinusA) || !lte(C, aPlusB)) {
            return ESat.FALSE;
         }
      } else if (gt(A, 0) && gte(A, B)) { // A > 0 ^ 0 < B <= A => A - B <= C <= A + B
         int aMinusB = A.getLB() - B.getUB();
         int aPlusB = A.getUB() + B.getUB();
         if (!gte(C, aMinusB) || !lte(C, aPlusB)) {
            return ESat.FALSE;
         }
      }

      return ESat.UNDEFINED;
   }
}
