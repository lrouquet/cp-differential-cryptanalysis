package fr.limos.decrypt.propagators;

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;

public class MDSPropagator extends Propagator<IntVar> {

    private final IntVar Y;
    private final IntVar Z;

    public MDSPropagator(IntVar sumColY, IntVar sumColZ) {
        super(new IntVar[]{sumColY, sumColZ}, PropagatorPriority.BINARY, false);
        checkDomain(sumColY);
        this.Y = sumColY;
        checkDomain(sumColZ);
        this.Z = sumColZ;
    }

    private void checkDomain(IntVar variable) {
        if (variable.getLB() != 0)
            throw new RuntimeException("Invalid lower bound for " + variable.getName() + " in MDS propagator");
        if (variable.getUB() != 4)
            throw new RuntimeException("Invalid upper bound for " + variable.getName() + " in MDS propagator");
    }

    public boolean reduce(IntVar from, IntVar to) throws ContradictionException {
        boolean anyUpdate = false;
        if (from.isInstantiatedTo(0)) {
            // dom(from) = [0, 0]
            to.instantiateTo(0, this);
        } else if (from.getLB() > 0) {
            // dom(from) = [>=1, n]
            if (from.isInstantiatedTo(1)) {
                // dom(from) == [1, 1]
                to.instantiateTo(4, this);
            } else {
                // dom(from) != [1, 1]
                if (5 - from.getUB() > to.getLB()) {
                    anyUpdate |= to.updateLowerBound(5 - from.getUB(), this);
                }
            }
        } else {
            // dom(from) = [>=0, n]
            if (from.getUB() < 4) {
                anyUpdate |= to.removeInterval(1, 4 - from.getUB(), this);
            }
        }
        return anyUpdate;
    }

    @Override public void propagate(int evtmask) throws ContradictionException {
        while (reduce(Y, Z) || reduce(Z, Y)) {}
        if (Y.getLB() + Z.getLB() >= 5) { setPassive(); }
    }

    @Override public ESat isEntailed() {
        if (Y.isInstantiated() && Z.isInstantiated()) {
            int sum = Y.getValue() + Z.getValue();
            if (sum == 1 || sum == 2 || sum == 3 || sum == 4) {
                return ESat.FALSE;
            } else {
                return ESat.TRUE;
            }

        } else {
            if (Y.getLB() + Z.getLB() >= 5) {
                return ESat.TRUE;
            }

            return ESat.UNDEFINED;
        }
    }
}
