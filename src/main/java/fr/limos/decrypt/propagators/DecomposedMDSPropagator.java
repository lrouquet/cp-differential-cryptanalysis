package fr.limos.decrypt.propagators;

import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.util.ESat;

import java.util.Arrays;

public class DecomposedMDSPropagator extends Propagator<BoolVar> {

    public DecomposedMDSPropagator(BoolVar[] variables) {
        super(variables, PropagatorPriority.LINEAR, false);
        checkArguments(variables);
    }

    private void checkArguments(BoolVar[] vars) {
        if (vars.length != 8) throw new RuntimeException("Invalid array size, given: " + vars.length + ", expected: 8");
    }

    @Override public void propagate(int evtmask) throws ContradictionException {
        int nbTrueAndFalse = nbTrueAndFalse();
        int nbTrue = nbTrueAndFalse >> 16;
        int nbFalse = nbTrueAndFalse & 0xFFFF;
        if (nbTrue != 0 && nbFalse > 3) {
            fails();
        } else if (nbTrue != 0 && nbFalse == 3) {
            setAllUndefinedToTrue();
        } else if (nbFalse > 3) {
            setAllUndefinedToFalse();
        } else if (nbTrue >= 5) {
            setPassive();
        }
    }

    private void setAllUndefinedToFalse() throws ContradictionException {
        assert Arrays.stream(vars).noneMatch(it -> it.isInstantiatedTo(1));
        for (int i = 0; i < 8; i++) {
            if (!vars[i].isInstantiated(    )) {
                vars[i].setToFalse(this);
            }
        }
    }

    private void setAllUndefinedToTrue() throws ContradictionException {
        assert Arrays.stream(vars).filter(it -> it.isInstantiatedTo(0)).count() <= 3L;
        for (int i = 0; i < 8; i++) {
            if (!vars[i].isInstantiated()) {
                vars[i].setToTrue(this);
            }
        }
    }

    private int nbTrueAndFalse() {
        int nbTrue = 0;
        int nbFalse = 0;
        for (int i = 0; i < vars.length; i++) {
            if (vars[i].isInstantiated()) {
                if (vars[i].getValue() == 1) {
                    nbTrue += 1;
                } else {
                    nbFalse += 1;
                }
            }
        }
        return (nbTrue << 16) + nbFalse;
    }

    @Override public ESat isEntailed() {
        int nbTrueAndFalse = nbTrueAndFalse();
        int nbTrue = nbTrueAndFalse >> 16;
        int nbFalse = nbTrueAndFalse & 0xFFFF;
        if (nbTrue + nbFalse == 8) {
            if (nbTrue == 1 || nbTrue == 2 || nbTrue == 3 || nbTrue == 4) {
                return ESat.FALSE;
            } else {
                return ESat.TRUE;
            }
        } else {
            if (nbTrue != 0 && nbFalse > 3) {
                return ESat.FALSE;
            }
            return ESat.UNDEFINED;
        }
    }
}
