package fr.limos.decrypt.ciphering;

public class BlockDimensions {

    public final int bits;
    public final int nbRows;
    public final int nbCols;

    public BlockDimensions(int bits, int nbRows, int nbCols) {
        this.bits = bits;
        this.nbRows = nbRows;
        this.nbCols = nbCols;
    }

}
