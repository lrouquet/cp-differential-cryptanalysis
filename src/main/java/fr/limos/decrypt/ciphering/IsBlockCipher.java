package fr.limos.decrypt.ciphering;

public interface IsBlockCipher {

    BlockDimensions textBlock();

    default int textCols() {
        return textBlock().nbCols;
    }

    default int textRows() {
        return textBlock().nbRows;
    }

    BlockDimensions keyBlock();

    default int keyCols() {
        return keyBlock().nbCols;
    }

    default int keyRows() {
        return keyBlock().nbRows;
    }

    int getSeqSize();

    default int getMaxValue() {
        return (1 << getSeqSize()) - 1;
    }
}
