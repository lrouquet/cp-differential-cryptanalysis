package fr.limos.decrypt.ciphering;

public interface IsIterativeModel {

    int getNbRounds();

    default int getLastRound() {
        return getNbRounds() - 1;
    }

}