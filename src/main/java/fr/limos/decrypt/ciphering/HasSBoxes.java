package fr.limos.decrypt.ciphering;

import org.chocosolver.solver.constraints.extension.Tuples;

public interface HasSBoxes {

    Tuples createRelationSBoxes();

    Tuples createRelationSBoxesNoP();

}
