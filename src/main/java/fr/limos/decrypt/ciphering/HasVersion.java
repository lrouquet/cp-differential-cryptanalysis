package fr.limos.decrypt.ciphering;

public interface HasVersion<V> {

    V getVersion();

}
