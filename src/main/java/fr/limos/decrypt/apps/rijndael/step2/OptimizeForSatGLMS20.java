package fr.limos.decrypt.apps.rijndael.step2;

import com.github.rloic.phd.core.arrays.IntMatrix;
import com.github.rloic.phd.core.arrays.Matrix;
import com.github.rloic.phd.core.arrays.Tensor3;
import com.github.rloic.phd.core.cryptography.rijndael.differential.relatedkey.step1.enumeration.Solution;
import fr.limos.decrypt.ciphers.rijndael.Rijndael;
import fr.limos.decrypt.ciphers.rijndael.RijndaelGLMS20;
import fr.limos.decrypt.models.CryptoModelStep2;
import fr.limos.decrypt.utils.IntPtr;
import fr.limos.decrypt.utils.ValuesExtractor;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMax;
import org.chocosolver.solver.search.strategy.selectors.variables.DomOverWDegRef;
import org.chocosolver.solver.variables.IntVar;

import java.util.ArrayList;
import java.util.List;

import static com.github.rloic.phd.core.arrays.MatrixKt.matrixOfNulls;
import static com.github.rloic.phd.core.arrays.Tensor3Kt.tensor3OfNulls;
import static fr.limos.decrypt.apps.rijndael.step2.CommonKt.displayConcreteSolution;

@SuppressWarnings("NonAsciiCharacters")
public class OptimizeForSatGLMS20 {

    public OptimizeForSatGLMS20(
            Solution step1Solution,
            IntPtr nbSol, IntPtr bestProp, IntPtr bestObjStep1) {

        final String name = "Rinjdael/Step2/OptimizeForSatGLMS20";
        final CryptoModelStep2<Rijndael> m = new CryptoModelStep2<>(name, new Rijndael(step1Solution.getConfig()), step1Solution.getConfig().getNr());

        final int Nr = step1Solution.getConfig().getNr();
        final int Nb = step1Solution.getConfig().getNb();
        final int NRows = 4;

        List<IntVar> probabilities = new ArrayList<>();
        Tensor3<IntVar> δX = tensor3OfNulls(Nr, NRows, Nb);
        Tensor3<IntVar> pX = tensor3OfNulls(Nr, NRows, Nb);
        for (int i = 0; i < Nr; i++) {
            for (int j = 0; j < NRows; j++) {
                for (int k = 0; k < Nb; k++) {
                    if (step1Solution.getΔX().get(i, j, k) == 1) {
                        δX.set(i, j, k, m.intVar(1, m.getMaxValue()));
                        pX.set(i, j, k, m.intVar(-7, -6));
                    } else {
                        δX.set(i, j, k, m.ZERO);
                        pX.set(i, j, k, m.ZERO);
                    }
                }
            }
        }

        Tensor3<IntVar> δSX = tensor3OfNulls(Nr, NRows, Nb);
        Tensor3<IntVar> δY = tensor3OfNulls(Nr, NRows, Nb);
        Tensor3<IntVar> δZ = tensor3OfNulls(Nr - 1, NRows, Nb);

        Matrix<IntVar> pWK = matrixOfNulls(NRows, Nb * Nr);
        Matrix<IntVar> δWK = generateExpandedKey(m, step1Solution.getΔWK(), pWK);

        for (int r = 0; r < Nr - 1; r++) {
            δSX.set(r, m.subCell(δX.get(r), pX.get(r)));
            δY.set(r, m.shiftRows(Rijndael::textBlock, Rijndael::shiftOffsets, δSX.get(r)));
            δZ.set(r, RijndaelGLMS20.mixColumn(m, δY.get(r)));
            for (int j = 0; j < NRows; j++) {
                for (int k = 0; k < Nb; k++) {
                    m.postXor(δX.get(r + 1, j, k), δZ.get(r, j, k), δWK.get(j, (r + 1) * Nb + k));
                }
            }
        }

        for (int j = 0; j < NRows; j++) {
            for (int k = 0; k < Nb; k++) {
                δSX.set(Nr - 1, m.subCell(δX.get(Nr - 1), pX.get(Nr - 1)));
            }
            δY.set(Nr - 1, m.shiftRows(Rijndael::textBlock, Rijndael::shiftOffsets, δSX.get(Nr - 1)));
        }

        for (int i = 0; i < Nr; i++) {
            for (int j = 0; j < NRows; j++) {
                for (int k = 0; k < Nb; k++) {
                    if (pX.get(i, j, k) != m.ZERO) {
                        assert step1Solution.getΔX().get(i, j, k) == 1;
                        probabilities.add(pX.get(i, j, k));
                    }
                }
            }
        }

        for (int k = 0; k < Nb * Nr; k++) {
            if (m.delegateCipher.isSbColumn(k)) {
                for (int j = 0; j < NRows; j++) {
                    if (pWK.get(j, k) != m.ZERO) {
                        assert step1Solution.getΔWK().get(j, k) == 1;
                        probabilities.add(pWK.get(j, k));
                    }

                }
            }
        }

        IntVar probabilitiesSum = m.intVar(-7 * step1Solution.getConfig().getObjStep1(), -6 * step1Solution.getConfig().getObjStep1());
        m.sum(probabilities.toArray(new IntVar[]{}), "=", probabilitiesSum).post();
        m.delegateModel.setObjective(true, probabilitiesSum);

        List<IntVar> order = new ArrayList<>();
        for (int i = 0; i < Nr; i++) {
            for (int k = 0; k < Nb; k++) {
                for (int j = 0; j < 4; j++) {
                    if (i < Nr - 1) {
                        order.add(δY.get(i, j, k));
                    }
                    if (m.delegateCipher.isSbColumn(i * Nb + k)) {
                        order.add(δWK.get(j, i * Nb + k));
                    }
                }
            }
        }

        for (int i = 0; i < Nr - 1; i++) {
            for (int k = 0; k < Nb; k++) {
                for (int j = 0; j < 4; j++) {
                    order.add(δZ.get(i, j, k));
                }
            }
        }

        for (IntVar v : m.delegateModel.retrieveIntVars(true)) {
            if (!order.contains(v)) order.add(v);
        }

        Solver s = m.getSolver();
        s.setSearch(Search.lastConflict(new DomOverWDegRef(order.toArray(new IntVar[]{}), 0L, new IntDomainMax())));
        s.plugMonitor((IMonitorSolution) () -> {
            if (probabilitiesSum.getValue() > bestProp.value) {
                bestProp.value = probabilitiesSum.getValue();
                nbSol.value += 1;
                System.out.println("  New best probability: 2^{" + bestProp.value + "}");
                bestObjStep1.value = step1Solution.getConfig().getObjStep1();

                displayConcreteSolution(
                        Nr,
                        m.delegateCipher,
                        ValuesExtractor.values(δWK),
                        ValuesExtractor.values(δX),
                        ValuesExtractor.values(δSX),
                        ValuesExtractor.values(δY),
                        ValuesExtractor.values(δZ),
                        4
                );

            }
        });

        //noinspection StatementWithEmptyBody
        while (s.solve()) {
        }
        System.out.println("    NO BETTER SOLUTION");
    }

    private Matrix<IntVar> generateExpandedKey(CryptoModelStep2<Rijndael> m, IntMatrix ΔWK, Matrix<IntVar> pWK) {
        final int Nk = m.keyCols();
        final int NRows = m.keyRows();
        final int Nb = m.textCols();
        final int Nr = m.getNbRounds();

        Matrix<IntVar> δWK = matrixOfNulls(NRows, Nb * Nr);
        for (int j = 0; j < Nb * Nr; j++) {
            if (m.delegateCipher.isSbColumn(j)) {
                for (int i = 0; i < NRows; i++) {
                    if (ΔWK.get(i, j) == 1) {
                        δWK.set(i, j, m.intVar(1, m.getMaxValue()));
                        pWK.set(i, j, m.intVar(-7, -6));
                    } else {
                        δWK.set(i, j, m.ZERO);
                        pWK.set(i, j, m.ZERO);
                    }
                }
            } else {
                for (int i = 0; i < NRows; i++) {
                    δWK.set(i, j, m.byteVar());
                    pWK.set(i, j, null);
                }
            }
        }

        for (int j = Nk; j < Nb * Nr; j++) {
            if (j % Nk == 0) {
                for (int i = 0; i < NRows; i++) {
                    m.postXor(δWK.get(i, j), δWK.get(i, j - Nk), m.subCell(δWK.get((i + 1) % NRows, j - 1), pWK.get((i + 1) % NRows, j - 1)));
                }
            } else if ((Nk > 6) && (j % Nk == 4)) {
                for (int i = 0; i < NRows; i++) {
                    m.postXor(δWK.get(i, j), δWK.get(i, j - Nk), m.subCell(δWK.get(i, j - 1), pWK.get(i, j - 1)));
                }
            } else {
                for (int i = 0; i < NRows; i++) {
                    m.postXor(δWK.get(i, j), δWK.get(i, j - Nk), δWK.get(i, j - 1));
                }
            }

        }

        return δWK;
    }

}
