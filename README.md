# And Rijndael ?

## The project

The project is done in [Java](https://docs.oracle.com/en/java/index.html) and [Kotlin](https://kotlinlang.org/). 
It will be release at [https://gitlab.inria.fr/lrouquet/public-paper-fse-2020]().

### Dependencies

The project require :
- Java >= 8
- Gradle >= 6 ([https://gradle.org/]())
- MiniZinc >= 2.4.3 ([https://www.minizinc.org/]())
- Picat >= 2.8.3 ([http://picat-lang.org/download.html]())

### Build command

The build command to generate the `.jar` file is :
```bash
gradle build
```

The `.jar` file will be located in the `build/libs` folder.

### Run command

#### Step1

To execute the step1 you must have MiniZinc and Picat on your machine.
The command line to solve the step1 is :

```bash
java -cp build/libs/CP-differential-cryptanalysis-1.0-SNAPSHOT.jar fr.limos.decrypt.apps.rijndael.step1.EnumerateSatKt \
CIPHER_BITS=CB KEY_BITS=KB Nr=NR objStep1=O1 \
MZN2FZN=MZN2FZN_PATH \
PICAT=PICAT_PATH \
FZN_PICAT_SAT=FZN_PICAT_SAT_PATH
```

Where `CB` is the cipher block size in bits (128, 160, 192, 224 or 256), 
`KB` is the key block size in bits (128, 160, 192, 224 or 256), 
NR is the number of rounds and O1 is the number of rounds.

MZN2FZN_PATH is the path to the file `mzn2fzn` or the the `minizinc` of the MiniZinc library (**including** the filename).
PICAT_PATH is the path to the `picat` executable.
FZN_PICAT_SAT_PATH is the path to the `picat_sat.pi` file ([available here](http://picat-lang.org/flatzinc/fzn_picat_sat.pi)). Note that this extension also requires the two following files : [http://picat-lang.org/flatzinc/fzn_parser.pi]() and [http://picat-lang.org/flatzinc/fzn_tokenizer.pi]().

Exemple of use:
```bash
java -cp build/libs/CP-differential-cryptanalysis-1.0-SNAPSHOT.jar fr.limos.decrypt.apps.rijndael.step1.EnumerateSatKt \
CIPHER_BITS=128 KEY_BITS=128 Nr=3 objStep1=5 \
MZN2FZN="/home/rloic/.local/lib/MiniZinc/2.4.3/bin/mzn2fzn" \
PICAT="/home/rloic/.local/lib/Picat/3.0/picat" \
FZN_PICAT_SAT="/home/rloic/.local/lib/Picat/3.0/lib/fzn_picat_sat.pi"
```

**IMPORTANT**
Note that the execution of step1 use `java` but it is only a wrapper around the execution of `Picat` used to forbid old solutions and to have a better rendering. The main model is written in Minizinc under the `sat` folder.

#### Step1 and 2

To execute the step1 you must have MiniZinc and Picat on your machine.
The command line to solve the step2 is :

```bash
java -cp build/libs/CP-differential-cryptanalysis-1.0-SNAPSHOT.jar fr.limos.decrypt.apps.rijndael.step2.OptimizeSat \
CIPHER_BITS=CB KEY_BITS=KB Nr=NR objStep1=O1 \
MZN2FZN=MZN2FZN_PATH \
PICAT=PICAT_PATH \
FZN_PICAT_SAT=FZN_PICAT_SAT_PATH
```

For the `MZN2FZN_PATH`, `PICAT_PATH` and `FZN_PICAT_SAT_PATH` variables see the section Step1.

### File Descriptions

The `sat/rijndael.mzn` is the main model of the step1. It is written in Minizinc and corresponds to a partial version of the Advanced model described in the paper.
The script `src/main/kotlin/fr/limos/decrypt/apps/rijndael/step1/helpers/KeySchedule.kt` is used to generated constraints on the ExtXOR set.

The script `src/main/kotlin/fr/limos/decrypt/apps/rijndael/step1/MinimizeIter.kt` is used to find the minimal number of S-Boxes for a given configuration (CIPHER_BITS and KEY_BITS).
It will print the minimal number required of S-Boxes for each rounds.

The script `src/main/kotlin/fr/limos/decrypt/apps/rijndael/step1/EnumerateSat.kt` enumerates all the step1 solution for a given instance.
The script `src/main/kotlin/fr/limos/decrypt/apps/rijndael/step2/OptimizeSat.kt` will given the best probability for the step2. This script mix the step1 and the step2 processes.